<?php

/**
 * @file
 * Hooks provided by the pagarme module.
 */

/**
 * Make changes to the metadata of the one sent by the transaction before it is sent to the Pagar.me.
 *
 * @param $metadata
 *   Additional data passed in the transaction creation to later filter on the dashboard Pagar.me.
 * @param $order
 *   The order that will be processed by the checkout.
 */
function hook_pagarme_metadata_alter(&$metadata, &$order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $metadata['commerce_line_items'] = array();
  foreach ($order_wrapper->commerce_line_items->value() as $line_item) {
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    if ($line_item_wrapper->type->value() == 'product') {
      $item = array();
      $item['sku'] = $line_item_wrapper->line_item_label->value();
      $item['quantity'] = $line_item_wrapper->quantity->value();
      $item['unit_price'] = $line_item_wrapper->commerce_unit_price->amount->value();
      $metadata['commerce_line_items'][] = $item;
    }
  }
}

/**
 * Answer Pagar.me with card_hash
 * 
 * @param $pagarme_answer
 *   Answer Pagar.me
 * @param $order
 *   The order that will be processed by the checkout.
 */
function hook_pagarme_checkout_answer(&$pagarme_answer, &$order) {
  // No example.
}

/**
 * Allow modules to execute business logic when a transaction is concluded by Pagar.me.
 *
 * @param $transaction \PagarMe\Sdk\Transaction\CreditCardTransaction
 * Transaction object returned by Pagarme SDK
 */
function hook_pagarme_transaction_post_process(&$transaction, &$order) {
  // No example.
}

/**
 * Allow modules to run some business logic every time you receive a POSTback from Pagar.me.
 *
 * @param $postback
 *   Data sent via POSTback of a transaction by Pagar.me
 */
function hook_pagarme_preprocess_postback(&$postback) {
  // No example.
}
