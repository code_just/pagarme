(function() {
  (function($) {
    Drupal.behaviors.pagarmePaymentMethods = {
      payment_methods: ['pagarme_cp|commerce_payment_pagarme_cp', 'pagarme_whitelabel|commerce_payment_pagarme_whitelabel'],
      attach: function(context, settings) {
        var self = this;

        $("#edit-commerce-payment-payment-method.form-radios input", context).change(function() {
          if ($.inArray($(this).val(), self.payment_methods) !== -1) {
            $("#edit-buttons input.checkout-continue").hide();
            $("#edit-buttons").addClass("pagarme");
          } else {
            $("#edit-buttons input.checkout-continue").show();
            $("#edit-buttons").removeClass("pagarme");
          }
        });

        $('#pagarme-pay-button').click(function(e) {
          e.preventDefault();

          // Iniciar a instância do checkout e declarar um callback de sucesso
          var checkout = new PagarMeCheckout.Checkout({
            "encryption_key": Drupal.settings.pagarme.encryption_key,
            success: Drupal.Pagarme.CP.sendSuccesfull
          });

          // Passar pârametros e abrir o modal (é necessário passar os valores boolean em "var params" como string)
          checkout.open(Drupal.settings.pagarme.checkout_params);
        });

        $('#pagarme-whitelabel-credit-card-pay-button').click(function(e) {
          e.preventDefault();

          PagarMe.encryption_key = Drupal.settings.pagarme.encryption_key;

          // inicializa um objeto de cartão de crédito e completa
          // com os dados do form
          var creditCard = new PagarMe.creditCard();
          var sufix = 'edit-commerce-payment-payment-details-pagarme-whitelabel-credit-card';
          creditCard.cardHolderName = $('#'+sufix+'-name').val();
          creditCard.cardExpirationMonth = $('#'+sufix+'-month-expiration').val();
          creditCard.cardExpirationYear = $('#'+sufix+'-year-expiration').val();
          creditCard.cardNumber = $('#'+sufix+'-number').val();
          creditCard.cardCVV = $('#'+sufix+'-cvv').val();
          
          // pega os erros de validação nos campos do form
          var fieldErrors = creditCard.fieldErrors();
          
          //Verifica se há erros
          var hasErrors = false;
          for(var field in fieldErrors) { hasErrors = true; break; }

          if(hasErrors) {
              // realiza o tratamento de errors
              Drupal.Pagarme.UI.creditCardErrorAlert(fieldErrors);
            } else {
              // se não há erros, gera o card_hash...
              creditCard.generateHash(function(cardHash) {
                Drupal.Pagarme.answer = JSON.stringify({
                  pagarme_payment_method: 'credit_card',
                  card_hash: cardHash
                });
                Drupal.Pagarme.submitCheckoutForm();
              });
            }
            return false;
        });

        $('#pagarme-whitelabel-bankbillet-pay-button').click(function(e) {
          e.preventDefault();
          Drupal.Pagarme.answer = JSON.stringify({pagarme_payment_method: 'boleto'});
          Drupal.Pagarme.submitCheckoutForm();
        });
      }
    };
    $("#edit-commerce-payment-payment-details-pagarme-cp, #edit-commerce-payment-payment-details-pagarme-whitelabel").ready(function() {
      $("#edit-buttons input.checkout-continue").hide();
      $("#edit-buttons").addClass("pagarme");
    });

    Drupal.Pagarme = {};
    Drupal.Pagarme.CP = {};

    //Tratar aqui as ações de callback do checkout, como exibição de mensagem ou envio de token para captura da transação
    Drupal.Pagarme.CP.sendSuccesfull = function(data) {
      Drupal.Pagarme.answer = JSON.stringify(data);
      Drupal.Pagarme.submitCheckoutForm();
    };

    Drupal.Pagarme.submitCheckoutForm = function() {
      if (Drupal.Pagarme.answer.length !== 0) {
        $("#payment-details .pagarme-cp-answer").val(Drupal.Pagarme.answer);
        $("#pagarme-pay-button").remove();
        $("#payment-details").parents("form").submit();
      }
    };

    Drupal.Pagarme.UI = {};

    Drupal.Pagarme.UI.messageAlert = function(message) {
      $("#pagarme-cp-messages").empty();
      $("#pagarme-cp-messages").removeClass();
      $("#pagarme-cp-messages").append(message);
      $("#pagarme-cp-messages").show({
        effect: "highlight"
      });
    };

    Drupal.Pagarme.UI.creditCardErrorAlert = function(fieldErrors) {
      var message = '';
      $.each(Object.keys(fieldErrors), function(index, field) {
        message += fieldErrors[field] + '<br />';
      });
      console.log(message);
      Drupal.Pagarme.UI.errorAlert(message);
    };

    Drupal.Pagarme.UI.externalPaymentUrlAlert = function(data) {
      var url = '';
      var message = Drupal.t('<p>To complete your payment, <a data-href="!link">click here</a> to open your billet in another browser window.</p><p>You will be redirected automatically after this.</p>', {"!link": url});
      Drupal.Pagarme.UI.infoAlert(message);
      $("#pagarme-cp-messages a").click(function() {
        window.open($(this).data("href"));
        Drupal.Pagarme.CP.submitCheckoutForm();
      });
    };

    Drupal.Pagarme.UI.errorAlert = function(message) {
      Drupal.Pagarme.UI.messageAlert(message);
      $("#pagarme-cp-messages").addClass("messages error");
    };

    Drupal.Pagarme.UI.infoAlert = function(message) {
      Drupal.Pagarme.UI.messageAlert(message);
      $("#pagarme-cp-messages").addClass("info");
    };

  })(jQuery);

}).call(this);
