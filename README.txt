README.txt
==========

Este módulo integra o Pagar.me (https://pagar.me/) ao Drupal Commerce.
O módulo pagarme implementa o checkout transparente oferecido pela Pagar.me (https://pagar.me/checkout/) customizável às suas necessidades e padrões de design.

REQUERIMENTOS
=============

Este módulo tem como dependência a Pagar.me PHP SDK Library versão v3 (https://github.com/pagarme/pagarme-php) desenvolvido pelo Pagar.me, baseada nos recursos oferecidos pela Pagar.me API (https://docs.pagar.me/api/).

A biblioteca deve ser instalada em sites/all/libraries, no sub-diretório /pagarme-php.

*** ENGLISH ***

This module integrates Pagar.me (https://pagar.me/) into Drupal Commerce.
The payme module implements the transparent checkout offered by Pagar.me (https://pagar.me/checkout/) customizable to your needs and design standards.

REQUIREMENTS
============

This module relies on the Pagar.me PHP SDK Library version v3 (https://github.com/pagarme/pagarme-php) developed by Pagar.me, based on the features offered by Pagar.me API (https://docs.pagar.me/api/).

The library should be installed on sites/all/libraries, in the /pagename-php subdirectory.
