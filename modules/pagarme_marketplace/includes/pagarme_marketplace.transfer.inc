<?php 

/**
 * @file
 * Transfer implementation.
 */

/**
 * The transfer definition.
 *
 * @return array
 */
function _pagarme_marketplace_transfer_menu_definition() {
  $items = array();

  $items['admin/commerce/config/marketplace/transfers'] = array(
    'title' => t('Transferências'),
    'description' => 'Gestão de transferências.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_transfers'),
    'access arguments' => array('administer transfer'),
    'file' => 'includes/pagarme_marketplace.transaction.inc',
  );
  return $items;
}

function pagarme_marketplace_transfers($form, $form_state) {
  $parameters = drupal_get_query_parameters();
  $destination = drupal_get_destination();
  $page = (!empty($parameters['page'])) ? $parameters['page'] : 1;
  $per_page = 10;

  $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();
  $transfers = $pagarme_marketplace->pagarme->transfer()->getList($page, $per_page);

  $header = array(
    t('ID da transferência'),
    t('Valor'),
    t('Tipo'),
    t('Status'),
    t('Taxa'),
    t('Operações'),
  );

  $rows = array();
  foreach ($transfers as $transfer) {
    $row = array();
    $row['id'] = $transfer->getId();
    $row['amount'] = pagarme_currency_format($transfer->getAmount());
    $row['type'] = $transfer->getType();
    $status = $transfer->getStatus();
    $row['status'] = ($status == 'pending_transfer') ? t('transferência pendente') : $status;
    $row['fee'] = pagarme_currency_format($transfer->getFee());

    $operations = array();

    if ($transfer->getStatus() == 'pending_transfer') {
      $operations['refund'] = array(
        'title' => t('Cancelar transferência'),
        'href' => "admin/commerce/config/marketplace/transfers/cancel/" . $transfer->getId(),
        'query' => $destination,
      );
    }

    $row['operations'] = array(
      'data' => array(
        '#theme' => 'links',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline', 'nowrap')),
      ),
    );
    $rows[] = $row;
  }

  $form['transfers'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('Não existem transferências.'),
  );

  $form['pager'] = array(
    '#markup' => _pagarme_marketplace_pager_previous_next($parameters),
  );
  return $form;
}
