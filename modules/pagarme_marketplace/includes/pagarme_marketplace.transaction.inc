<?php 

/**
 * @file
 * Transaction implementation.
 */

/**
 * The transaction definition.
 *
 * @return array
 */
function _pagarme_marketplace_transaction_menu_definition() {
  $items = array();

  $items['admin/commerce/config/marketplace/transactions'] = array(
    'title' => t('Transações'),
    'description' => 'Gestão de transações.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_transactions'),
    'access arguments' => array('administer transaction'),
    'file' => 'includes/pagarme_marketplace.transaction.inc',
  );

  $items['admin/commerce/config/marketplace/transactions/refund/%'] = array(
    'title' => t('Estorno'),
    'description' => 'Estorno.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_transaction_refund', 6),
    'type' => MENU_CALLBACK,
    'access arguments' => array('transaction effect refund'),
    'file' => 'includes/pagarme_marketplace.transaction.inc',
  );

  return $items;
}

function pagarme_marketplace_transactions($form, $form_state) {
  $parameters = drupal_get_query_parameters();
  $destination = drupal_get_destination();
  $page = (!empty($parameters['page'])) ? $parameters['page'] : 1;
  $per_page = 10;

  $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();
  $transactions = $pagarme_marketplace->pagarme->transaction()->getList($page, $per_page);

  $header = array(
    t('ID da transação'),
    t('Status'),
    t('Date'),
    t('Forma de Pagamento'),
    t('Valor'),
    t('Valor Capturado'),
    t('Valor Estornado'),
    t('Operações'),
  );

  $rows = array();
  $status_readable_name = \Drupal\pagarme\Helpers\PagarmeUtility::statusReadableName();
  foreach ($transactions as $transaction) {
    $row = array();
    $row['id'] = $transaction->getId();
    $row['status'] = $status_readable_name[$transaction->getStatus()];
    $date_created = $transaction->getDateCreated()->getTimestamp();
    $row['date_created'] = format_date($date_created, 'short');
    $payment_method = $transaction->getPaymentMethod();
    $payment_method = ($payment_method == 'credit_card') ? t('cartão de crédito') : t('boleto');
    $row['payment_method'] = $payment_method;
    $row['amount'] = pagarme_currency_format($transaction->getAmount());
    $row['paid_mount'] = pagarme_currency_format($transaction->getPaidAmount());
    $row['refunded_amount'] = pagarme_currency_format($transaction->getRefundedAmount());
    $operations = array();

    if (user_access('transaction effect refund') && $transaction->getStatus() == PAGARME_STATUS_PAID) {
      $operations['refund'] = array(
        'title' => t('Efetuar estorno'),
        'href' => "admin/commerce/config/marketplace/transactions/refund/" . $transaction->getId(),
        'query' => $destination,
      );
    }

    $row['operations'] = array(
      'data' => array(
        '#theme' => 'links',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline', 'nowrap')),
      ),
    );
    $rows[] = $row;
  }

  $form['transactions'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('Não existem transações.'),
  );

  $form['pager'] = array(
    '#markup' => _pagarme_marketplace_pager_previous_next($parameters),
  );
  return $form;
}

function pagarme_marketplace_transaction_refund($form, $form_state, $transaction_id) {
  $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();
  $transaction = $pagarme_marketplace->pagarme->transaction()->get($transaction_id);

  $form = array('#id' => 'pagarme_marketplace_transaction_refund');

  $form['transaction_id'] = array(
    '#type' => 'hidden',
    '#value' => $transaction_id,
  );

  if ($transaction->getPaymentMethod() == 'boleto') {
    $form['bank_account'] = array(
      '#type' => 'fieldset',
      '#title' => t('Confirmação - dados bancários'),
    );

    $form['bank_account']['bank_code'] = array(
      '#type' => 'select',
      '#title' => 'Código do banco',
      '#description' => 'Código do banco do recebedor.',
      '#options' => \Drupal\pagarme\Helpers\PagarmeUtility::banks(),
      '#required' => TRUE,
    );

    $form['bank_account']['type'] = array(
      '#type' => 'select',
      '#title' => 'Tipo de conta',
      '#description' => 'Tipo de conta bancária.',
      '#options' => \Drupal\pagarme\Helpers\PagarmeUtility::accountTypes(),
      '#required' => TRUE,
    );

    $form['bank_account']['legal_name'] = array(
      '#type' => 'textfield',
      '#title' => 'Nome completo ou razão social',
      '#description' => 'Nome completo ou razão social do recebedor.',
      '#required' => TRUE,
    );

    $form['bank_account']['document_number'] = array(
      '#type' => 'textfield',
      '#title' => 'CPF ou CNPJ',
      '#description' => 'CPF ou CNPJ do recebedor.',
      '#element_validate' => array('element_validate_integer_positive'),
      '#maxlength' => 14,
      '#size' => 14,
      '#required' => TRUE,
    );

    $form['bank_account']['agencia'] = array(
      '#type' => 'textfield',
      '#title' => 'Número da agência',
      '#description' => 'Agência da conta do recebedor.',
      '#element_validate' => array('element_validate_integer_positive'),
      '#maxlength' => 5,
      '#size' => 5,
      '#required' => TRUE,
    );

    $form['bank_account']['agencia_dv'] = array(
      '#type' => 'textfield',
      '#title' => 'Dígito verificador da agência',
      '#description' => 'Dígito verificador da agência do recebedor.',
      '#maxlength' => 2,
      '#size' => 2,
    );

    $form['bank_account']['conta'] = array(
      '#type' => 'textfield',
      '#title' => 'Número da conta',
      '#description' => 'Número da conta bancária do recebedor.',
      '#element_validate' => array('element_validate_integer_positive'),
      '#maxlength' => 13,
      '#size' => 13,
      '#required' => TRUE,
    );

    $form['bank_account']['conta_dv'] = array(
      '#type' => 'textfield',
      '#title' => 'Dígito verificador da conta',
      '#description' => 'Dígito verificador da conta do recebedor.',
      '#maxlength' => 2,
      '#size' => 2,
      '#required' => TRUE,
    );
  } 
  else {
    $form['transaction_info'] = array(
      '#type' => 'fieldset',
      '#title' => t('Confirmação'),
    );

    $form['transaction_info']['notification'] = array(
      '#markup' => '<div><p>Tem certeza que deseja estornar essa transação</p><p><b>Essa operação é irreversível.<b></p></div>',
    );

    $paid_amount = $transaction->getPaidAmount();
    $refunded_amount = $transaction->getRefundedAmount();
    $refund_amount = $paid_amount - $refunded_amount;

    $description = "Valor a ser estornado. Valor máximo permitido " . pagarme_currency_format($refund_amount);
    $form['transaction_info']['refund'] = array(
      '#type' => 'textfield',
      '#title' => 'Valor a ser estornado',
      '#description' => $description,
      '#default_value' => commerce_currency_amount_to_decimal($refund_amount, commerce_default_currency()),
      '#required' => TRUE,
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirmar'),
    '#submit' => array('pagarme_marketplace_transaction_refund_submit'),
  );

  return $form;
}

function pagarme_marketplace_transaction_refund_validate($form, &$form_state) {
  $values = $form_state['values'];

  $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();
  $transaction = $pagarme_marketplace->pagarme->transaction()->get($values['transaction_id']);

  if ($transaction->getPaymentMethod() == 'credit_card') {
    $paid_amount = $transaction->getPaidAmount();
    $refunded_amount = $transaction->getRefundedAmount();
    $refund_amount = $paid_amount - $refunded_amount;

    $form_state['values']['refund'] = commerce_currency_decimal_to_amount(
        $form_state['values']['refund'],
        commerce_default_currency()
    );
    if ((int) $form_state['values']['refund'] > (int) $refund_amount) {
      form_set_error('refund', t('Valor a ser estornado é superior ao permitido.'));
    }
  }
}

function pagarme_marketplace_transaction_refund_submit($form, &$form_state) {
  $values = $form_state['values'];

  try {
    $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();
    $transaction = $pagarme_marketplace->pagarme->transaction()->get($values['transaction_id']);

    switch ($transaction->getPaymentMethod()) {
      case 'boleto':
        $data_bank_account = array(
          'bankCode' => $values["bank_code"],
          'type' => $values["type"],
          'legalName' => $values["legal_name"],
          'documentNumber' => $values["document_number"],
          'agencia' => $values["agencia"],
          'agenciaDv' => $values["agencia_dv"],
          'conta' => $values["conta"],
          'contaDv' => $values["conta_dv"],
        );

        $bank_account = new \PagarMe\Sdk\BankAccount\BankAccount($data_bank_account);
        $refunded_transaction = $pagarme_marketplace->pagarme->transaction()->boletoRefund(
            $transaction,
            $bank_account
        );
        break;
      case 'credit_card':
        $refunded_transaction = $pagarme_marketplace->pagarme->transaction()->creditCardRefund(
            $transaction,
            $values['refund']
        );
        break;
    }
    drupal_set_message(t('Estorno efetuado com sucesso.'));
  }
  catch (\PagarMe\Sdk\ClientException $e) {
    $response = json_decode(json_decode($e->getMessage()));
    $errors = array();
    if (!empty($response->errors)) {
      foreach ($response->errors as $key => $error) {
        $parameter_name = ($error->parameter_name) ? $error->parameter_name . ': ' : '';
        $errors[] = t($parameter_name) . t($error->message);
      }
    }
    $message = theme('item_list', array('items' => $errors));
    drupal_set_message(filter_xss_admin($message), 'error');
  } 
  catch (\Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
}