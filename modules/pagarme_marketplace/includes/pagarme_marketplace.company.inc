<?php 

/**
 * @file
 * Company implementation.
 */

/**
 * The company definition.
 *
 * @return array
 */
function _pagarme_marketplace_company_menu_definition() {
  $items = array();

  $items['admin/commerce/config/marketplace/company'] = array(
    'title' => t('Companhia'),
    'description' => 'Configurações e informações do recebedor principal.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_company'),
    'access arguments' => array('access company info'),
    'file' => 'includes/pagarme_marketplace.company.inc',
  );

  return $items;
}

function pagarme_marketplace_company($form, $form_state) {
  $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();
  $company_info = $pagarme_marketplace->getCompanyInfo();
  $balance = $pagarme_marketplace->pagarme->balance()->get();

  $form['company'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Saldo'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  $header = array(
    t('Valor a receber'),
    t('Valor disponível'),
    t('Valor já transferido')
  );

  $rows = array();
  $currency_code = commerce_default_currency();
  $currency = commerce_currency_load($currency_code);

  $waiting_funds = $balance->getWaitingFunds()->amount;
  $rows['data']['waiting_funds'] = pagarme_currency_format($waiting_funds);

  $available = $balance->getAvailable()->amount;
  $rows['data']['available'] =  pagarme_currency_format($available);

  $transferred = $balance->getTransferred()->amount;
  $rows['data']['transferred'] =  pagarme_currency_format($transferred);

  $form['company']['balance'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

   $form['recipient']['account'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Informações da conta'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

   // Default Recipient ID Pagarme.me
  $server = variable_get('pagarme_server');
  $default_recipient_id = $company_info->default_recipient_id->{$server};
  $default_recipient = $pagarme_marketplace->pagarme->recipient()->get($default_recipient_id);
  $rows = array();
  $rows[] = array(t('NOME/RAZÃO SOCIAL'), $default_recipient->getBankAccount()->getLegalName());
  $rows[] = array(t('BANCO'), $default_recipient->getBankAccount()->getBankCode());
  $rows[] = array(t('CPF/CNPJ'), $default_recipient->getBankAccount()->getDocumentNumber());
  $rows[] = array(t('AGÊNCIA'), $default_recipient->getBankAccount()->getAgencia());
  $rows[] = array(t('CONTA BANCÁRIA'), $default_recipient->getBankAccount()->getConta());

  $form['recipient']['account']['info'] = array(
    '#markup' => theme('table', array('rows' => $rows))
  );
  return $form;
}
