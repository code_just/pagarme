<?php 

/**
 * @file
 * Recipient implementation.
 */

/**
 * The recipient definition.
 *
 * @return array
 */
function _pagarme_marketplace_recipient_menu_definition() {
  $items = array();

  $items['admin/commerce/config/marketplace/recipients'] = array(
    'title' => t('Recebedores'),
    'description' => 'Lista de recebedores cadastrados no Pagar.me.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_recipient_list'),
    'access arguments' => array('administer recipient'),
    'file' => 'includes/pagarme_marketplace.recipient.inc',
  );

  $items['admin/commerce/config/marketplace/recipients/add'] = array(
    'title' => t('Adicionar recebedor'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_recipient_form'),
    'access arguments' => array('administer recipient'),
    'file' => 'includes/pagarme_marketplace.recipient.inc',
    'type' => MENU_LOCAL_ACTION,
  );

  $items['admin/commerce/config/marketplace/recipients/edit/%'] = array(
    'title' => t('Editar Recebedor'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_recipient_form', 5, 6),
    'access arguments' => array('administer recipient'),
    'file' => 'includes/pagarme_marketplace.recipient.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/commerce/config/marketplace/recipients/delete/%'] = array(
    'title' => t('Arquivar'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_recipient_delete_form', 6),
    'access arguments' => array('administer recipient'),
    'file' => 'includes/pagarme_marketplace.recipient.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/commerce/config/marketplace/recipients/balance/%'] = array(
    'title' => t('Saldo'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_recipient_balance_form', 6),
    'access arguments' => array('access recipient balance'),
    'file' => 'includes/pagarme_marketplace.recipient.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/commerce/config/marketplace/recipients/transfer/%'] = array(
    'title' => t('Realizar saque'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_recipient_transfer_form', 6),
    'access arguments' => array('access recipient transfer'),
    'file' => 'includes/pagarme_marketplace.recipient.inc',
    'type' => MENU_CALLBACK,
  );

  $items['pargame-marketplace/autocomplete/recipient'] = array(
    'page callback' => array('pagarme_marketplace_recipient_autocomplete'),
    'access arguments' => array('administer recipient'),
    'file' => 'includes/pagarme_marketplace.recipient.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function pagarme_marketplace_recipient_list() {
  $num_per_page = 10;
  $query_string = drupal_get_query_parameters();
  if (!empty($query_string['op']) && $query_string['op'] == t('Limpar')) {
    drupal_goto('admin/commerce/config/marketplace/recipients');
  }

  $query = db_select('pagarme_recipients', 'recipients')->extend('PagerDefault');
  $search_filter = '';
  $filter_type = '';
  $archived = (!empty($query_string['archived'])) ? $query_string['archived'] : '';
  if (empty($archived)) {
    $query->condition('archived', PAGARME_RECIPIENT_ARCHIVED, '<>');
  }
  if (!empty($query_string['search_filter'])) {
    $search_filter = $query_string['search_filter'];
    $filter_type = $query_string['filter_type'];
    $query->condition($filter_type, '%' . $search_filter . '%', 'LIKE');
  }

  $query->fields('recipients')
    ->limit($num_per_page)
    ->orderBy('changed', 'DESC');
  $recipients = $query->execute()->fetchAll();

  $header = array();
  $header[] = array(
    'data' => t('Nome completo / razão social'), 
    'field' => 'legal_name'
  );
  $header[] = array(
    'data' => t('CPF / CNPJ'), 
    'field' => 'document_number'
  );
  $header[] = array(
    'data' => t('Saque automático'), 
    'field' => 'transfer_enabled'
  );
  $header[] = array(
    'data' => t('Frequência'), 
    'field' => 'transfer_interval'
  );
  $header[] = array(
    'data' => t('Dia'), 
    'field' => 'transfer_day'
  );
  $header[] = array(
    'data' => t('Operações')
  );

  $rows = array();
  $destination = drupal_get_destination();
  foreach ($recipients as $item) {
    $row = array();
    $transfer_enabled = $item->transfer_enabled;
    $readable_transfer_interval = '';
    $readable_transfer_day = '';
    if ($transfer_enabled) {
      $transfer_interval = $item->transfer_interval;

      $transfer_interval_list = \Drupal\pagarme\Helpers\PagarmeUtility::transferInterval();
      $readable_transfer_interval = $transfer_interval_list[$transfer_interval];

      $readable_transfer_day = $transfer_day = $item->transfer_day;
      if ($transfer_interval == 'weekly') {
        $weekdays = \Drupal\pagarme\Helpers\PagarmeUtility::weekdays();
        $readable_transfer_day = $weekdays[$transfer_day];
      } 
    }
    $row['data']['legal_name'] = $item->legal_name;
    $row['data']['document_number'] = $item->document_number;
    $row['data']['transfer_enabled'] = ($transfer_enabled) ? t('SIM') : t('Não');
    $row['data']['transfer_interval'] = $readable_transfer_interval;
    $row['data']['transfer_day'] = $readable_transfer_day;

    $operations = array();
    $operations['edit'] = array(
      'title' => t('Editar'),
      'href' => "admin/commerce/config/marketplace/recipients/edit/" . $item->pagarme_id,
      'query' => $destination,
    );

    if (!$item->archived) {
      $operations['delete'] = array(
        'title' => t('Arquivar'),
        'href' => "admin/commerce/config/marketplace/recipients/delete/" . $item->recipient_id,
        'query' => $destination,
      );
    }

    if (user_access('access recipient balance')) {
      $operations['balance'] = array(
        'title' => t('Saldo'),
        'href' => "admin/commerce/config/marketplace/recipients/balance/" . $item->pagarme_id,
        'query' => $destination,
      );
    }

    if (user_access('access recipient transfer')) {
      $operations['transfer'] = array(
        'title' => t('Realizar saque'),
        'href' => "admin/commerce/config/marketplace/recipients/transfer/" . $item->pagarme_id,
        'query' => $destination,
      );
    }

    $row['data']['operations'] = array(
      'data' => array(
        '#theme' => 'links',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline', 'nowrap')),
      ),
    );

    $rows[] = $row;
  }

  $build = array();
  $build['#method'] = 'get';
  $build['search_filter'] = array(
    '#type' => 'textfield', 
    '#title' => t('Busca'),
    '#default_value' => $search_filter,
  );
  $build['filter_type'] = array(
    '#type' => 'select', 
    '#title' => t('Buscar por'),
    '#options' => array(
      'legal_name' => t('Nome completo / razão social'),
      'document_number' => t('CPF / CNPJ'),
    ),
    '#default_value' => $filter_type,
  );
  $build['archived'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Exibir recebedores arquivados'),
    '#default_value' => $archived,
  );
  $build['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Pesquisar'),
  );
  $build['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Limpar'),
  );
  $build['path_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('Não existem recebedores cadastrados.'),
  );
  $build['path_pager'] = array('#theme' => 'pager');

  return $build;
}

function pagarme_marketplace_recipient_form($form, $form_state, $op = 'add', $rcid =  NULL) {

  $form = array();
  $form = array('#id' => 'pagarme_marketplace_recipient_form');

  $form['rcid'] = array(
    '#type' => 'hidden',
    '#value' => $rcid
  );

  $form['transfer_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => 'Saque automático',
    '#description' => 'Indica se o recebedor pode receber os pagamentos automaticamente.',
  );

  $form['transfer_interval'] = array(
    '#type' => 'select',
    '#title' => 'Frequência na qual o recebedor irá ser pago',
    '#description' => 'Frequência na qual o recebedor irá ser pago.',
    '#options' => \Drupal\pagarme\Helpers\PagarmeUtility::transferInterval(),
    '#default_value' => 'weekly',
    '#ajax' => array(
      'callback' => '_pagarme_marketplace_transfer_day_ajax_callback',
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#states' => array(
      'visible' => array(
        'input[name="transfer_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['transfer_day'] = array(
    '#type' => 'select',
    '#title' => 'Dia no qual o recebedor vai ser pago',
    '#description' => 'Dia no qual o recebedor vai ser pago.',
    '#prefix' => '<div id="transfer-day-options-replace">',
    '#suffix' => '</div>',
    '#states' => array(
      'visible' => array(
        'input[name="transfer_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['bank_account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dados bancários do recebedor'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['bank_account']['bank_id'] = array('#type' => 'hidden');

  $form['bank_account']['bank_code'] = array(
    '#type' => 'select',
    '#title' => 'Código do banco',
    '#description' => 'Código do banco do recebedor.',
    '#options' => \Drupal\pagarme\Helpers\PagarmeUtility::banks(),
    '#required' => TRUE,
  );

  $form['bank_account']['type'] = array(
    '#type' => 'select',
    '#title' => 'Tipo de conta',
    '#description' => 'Tipo de conta bancária.',
    '#options' => \Drupal\pagarme\Helpers\PagarmeUtility::accountTypes(),
    '#required' => TRUE,
  );

  $form['bank_account']['legal_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Nome completo ou razão social',
    '#description' => 'Nome completo ou razão social do recebedor.',
    '#required' => TRUE,
  );

  $form['bank_account']['document_number'] = array(
    '#type' => 'textfield',
    '#title' => 'CPF ou CNPJ',
    '#description' => 'CPF ou CNPJ do recebedor.',
    '#element_validate' => array('element_validate_integer_positive'),
    '#maxlength' => 14,
    '#size' => 14,
    '#required' => TRUE,
  );

  $form['bank_account']['agencia'] = array(
    '#type' => 'textfield',
    '#title' => 'Número da agência',
    '#description' => 'Agência da conta do recebedor.',
    '#element_validate' => array('element_validate_integer_positive'),
    '#maxlength' => 5,
    '#size' => 5,
    '#required' => TRUE,
  );

  $form['bank_account']['agencia_dv'] = array(
    '#type' => 'textfield',
    '#title' => 'Dígito verificador da agência',
    '#description' => 'Dígito verificador da agência do recebedor.',
    '#maxlength' => 2,
    '#size' => 2,
  );

  $form['bank_account']['conta'] = array(
    '#type' => 'textfield',
    '#title' => 'Número da conta',
    '#description' => 'Número da conta bancária do recebedor.',
    '#element_validate' => array('element_validate_integer_positive'),
    '#maxlength' => 13,
    '#size' => 13,
    '#required' => TRUE,
  );

  $form['bank_account']['conta_dv'] = array(
    '#type' => 'textfield',
    '#title' => 'Dígito verificador da conta',
    '#description' => 'Dígito verificador da conta do recebedor.',
    '#maxlength' => 2,
    '#size' => 2,
    '#required' => TRUE,
  );

  if ($op == 'edit' && !empty($rcid)) {
    $obj_recipient = new \Drupal\pagarme_marketplace\PagarmeRecipient();
    $recipient = $obj_recipient->pagarme->recipient()->get($rcid);
    $form['transfer_enabled']['#default_value'] = $recipient->getTransferEnabled();
    $form['transfer_interval']['#default_value'] = $recipient->getTransferInterval();

    $form['transfer_day']['#default_value'] = $recipient->getTransferDay();
    $bank_account = $recipient->getBankAccount();

    $form['bank_account']['bank_id']['#value'] = $bank_account->getId();
    $form['bank_account']['bank_code']['#default_value'] = $bank_account->getBankCode();
    // $form['bank_account']['type']['#default_value'] = $bank_account->getType();
    $form['bank_account']['legal_name']['#default_value'] = $bank_account->getLegalName();
    $form['bank_account']['document_number']['#default_value'] = $bank_account->getDocumentNumber();
    $form['bank_account']['document_number']['#attributes'] = array('readonly' => 'readonly');
    $form['bank_account']['agencia']['#default_value'] = $bank_account->getAgencia();
    $form['bank_account']['agencia_dv']['#default_value'] = $bank_account->getAgenciaDv();
    $form['bank_account']['conta']['#default_value'] = $bank_account->getConta();
    $form['bank_account']['conta_dv']['#default_value'] = $bank_account->getContaDv();
  }

  $form['transfer_day']['#options'] = _pagarme_marketplace_transfer_day_options($form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Salvar recebedor'),
    '#submit' => array('pagarme_marketplace_recipient_form_submit')
  );

  return $form;
}

function pagarme_marketplace_recipient_form_validate($form, &$form_state) {
  $values =  $form_state['values'];
  if (!\Drupal\pagarme\Helpers\PagarmeCpfCnpj::valid($values['document_number'])) {
    form_set_error('document_number', t('O cpf/cnpj informado não é válido.'));
  }
}

function pagarme_marketplace_recipient_form_submit($form, &$form_state) {
  $transfer_interval = $form_state["values"]["transfer_interval"];
  $transfer_day = $form_state["values"]["transfer_day"];
  if ($transfer_interval ==  'daily') {
    $transfer_day = 0;
  }

  $transfer_enabled = TRUE;
  if (!$form_state["values"]["transfer_enabled"]) {
    $transfer_enabled = FALSE;
  }
  $values = array(
    'id' => $form_state["values"]["rcid"],
    'transferEnabled' => $transfer_enabled,
    'transferInterval' => $transfer_interval,
    'transferDay' => $transfer_day,
    'bankAccount' => array(
      'bankCode' => $form_state["values"]["bank_code"],
      'type' => $form_state["values"]["type"],
      'legalName' => $form_state["values"]["legal_name"],
      'documentNumber' => $form_state["values"]["document_number"],
      'agencia' => $form_state["values"]["agencia"],
      'agenciaDv' => $form_state["values"]["agencia_dv"],
      'conta' => $form_state["values"]["conta"],
      'contaDv' => $form_state["values"]["conta_dv"],
    )
  );

  try {
    pagarme_marketplace_recipient_save($values);
    drupal_set_message(t('Recebedor salvo com sucesso.'));
    drupal_goto('admin/commerce/config/marketplace/recipients');    
  } 
  catch (\PagarMe\Sdk\ClientException $e) {
    $response = json_decode(json_decode($e->getMessage()));
    $errors = array();
    if (!empty($response->errors)) {
      foreach ($response->errors as $key => $error) {
        $errors[] = t($error->parameter_name) . ': ' . t($error->message);
      }
    }
    $message = theme('item_list', array('items' => $errors));
    drupal_set_message(filter_xss_admin($message), 'error');
  } 
  catch (\Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
}

function pagarme_marketplace_recipient_delete_form($form, &$form_state, $rcid) {
  $form = array();
  $form = array('#id' => 'pagarme_marketplace_recipient_confirm_form');

  $form['rcid'] = array(
    '#type' => 'hidden',
    '#value' => $rcid,
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Arquivar'),
    '#submit' => array('pagarme_marketplace_recipient_delete'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('pagarme_marketplace_cancel'),
  );

  return $form;
}

function pagarme_marketplace_recipient_delete($form, &$form_state) {
  db_update('pagarme_recipients')
    ->fields(array('archived' => PAGARME_RECIPIENT_ARCHIVED))
    ->condition('recipient_id', $form_state['values']['rcid'])->execute();
  drupal_set_message(t('Recebedor arquivado.'));
}

function _pagarme_marketplace_transfer_day_ajax_callback($form, $form_state) {
  $commands = array();
  $commands[] = ajax_command_replace('#transfer-day-options-replace', render($form['transfer_day']));
  if ($form_state['values']['transfer_interval'] == 'daily') {
    $commands[] = ajax_command_invoke('#transfer-day-options-replace', 'hide');
  }
  return array('#type' => 'ajax', '#commands' => $commands);
}

function _pagarme_marketplace_transfer_day_options(&$form, $form_state) {
  $options = array();

  if (!empty($form_state['values']['transfer_interval'])) {
    $transfer_interval = $form_state['values']['transfer_interval'];
  } 
  elseif (!empty($form['transfer_interval']['#default_value'])) {
    $transfer_interval = $form['transfer_interval']['#default_value'];
  }

  if (!empty($transfer_interval)) {
    switch ($transfer_interval) {
      case 'daily':
        $options = array();
        $form['transfer_day']['#prefix'] = '<div id="transfer-day-options-replace" style="display:none">';
        break;
      case 'weekly':
        $options = \Drupal\pagarme\Helpers\PagarmeUtility::weekdays();
        break;
      case 'monthly':
        $options = \Drupal\pagarme\Helpers\PagarmeUtility::daysMonth();
        break;
    }
  }
  return $options;
}

function pagarme_marketplace_recipient_autocomplete($string = "") {
  $matches = array();
  if ($string) {
    $recipients = db_query('SELECT * FROM {pagarme_recipients} WHERE CONCAT(legal_name, document_number) LIKE :string LIMIT 10', array(':string' => '%' . $string . '%'))->fetchAll();

    foreach ($recipients as $recipient) {
      $key = $recipient->legal_name . ' [' . $recipient->recipient_id . ']';
      $description = 'Nome: ' . $recipient->legal_name;
      $description .= ' cpf/cnpj: ' . $recipient->document_number . '<hr>';
      $matches[$key] = '<div class="reference-autocomplete">' . $description . '</div>';
    }
  }
  drupal_json_output($matches);
}

function pagarme_marketplace_recipient_transfer_form($form, &$form_state, $recipient_id) {
  $currency_code = commerce_default_currency();

  $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();
  $company_info = $pagarme_marketplace->getCompanyInfo();


  $recipient = $pagarme_marketplace->pagarme->recipient()->get($recipient_id);

  $balance = $pagarme_marketplace->pagarme->recipient()->balance($recipient);
  $available = $balance->getAvailable()->amount;
  $available = pagarme_currency_format($available);

  $form = array('#id' => 'pagarme_marketplace_transfer_create');

  $options = array();

  $bank_account_id = $recipient->getBankAccount()->getId();
  $legal_name = $recipient->getBankAccount()->getLegalName();
  $options[$bank_account_id] = $legal_name;

   // Default Recipient ID Pagarme.me
  $server = variable_get('pagarme_server');
  $default_recipient_id = $company_info->default_recipient_id->{$server};
  $default_recipient = $pagarme_marketplace->pagarme->recipient()->get($default_recipient_id);
  $bank_account_id = $default_recipient->getBankAccount()->getId();
  $legal_name = $default_recipient->getBankAccount()->getLegalName();
  $options[$bank_account_id] = $legal_name;

  $form['bank_account_id'] = array(
    '#type' => 'select',
    '#title' => 'Selecione a conta',
    '#description' => 'Selecione a conta para a qual deseja efetuar o saque.',
    '#options' => $options,
  );

  $form['recipient_id'] = array(
    '#type' => 'hidden',
    '#value' => $recipient_id,
  );

  $form['transfer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Realizar saque'),
  );

  $currency = commerce_currency_load($currency_code);
  $amount = $balance->getAvailable()->amount;
  $amount = commerce_currency_amount_to_decimal($amount, $currency_code);
  $form['transfer']['amount'] = array(
    '#type' => 'textfield',
    '#title' => 'Escolha o valor',
    '#description' =>  filter_xss_admin('Valor máximo a ser transferido ' . $available),
    '#default_value' => $amount,
    '#element_validate' => array('element_validate_number'),
    '#field_prefix' => $currency['symbol'],
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirmar saque'),
    '#submit' => array('pagarme_marketplace_recipient_transfer_form_submit'),
  );

  $form['recipient_account'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Informações da conta do recebedor'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  $rows = array();
  $rows[] = array(t('NOME/RAZÃO SOCIAL'), $recipient->getBankAccount()->getLegalName());
  $rows[] = array(t('BANCO'), $recipient->getBankAccount()->getBankCode());
  $rows[] = array(t('CPF/CNPJ'), $recipient->getBankAccount()->getDocumentNumber());
  $rows[] = array(t('AGÊNCIA'), $recipient->getBankAccount()->getAgencia());
  $rows[] = array(t('CONTA BANCÁRIA'), $recipient->getBankAccount()->getConta());

  $form['recipient_account']['info'] = array(
    '#markup' => theme('table', array('rows' => $rows))
  );

  return $form;
}

function pagarme_marketplace_recipient_transfer_form_validate($form, &$form_state) {
  $amount = &$form_state['values']['amount'];
  $recipient_id = $form_state['values']['recipient_id'];
  $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();
  $balance = $pagarme_marketplace->pagarme->recipient()->balance(
      new \PagarMe\Sdk\Recipient\Recipient(array('id' => $recipient_id))
  );

  $amount = commerce_currency_decimal_to_amount($amount, commerce_default_currency());
  if ((int) $amount > (int) $balance->getAvailable()->amount) {
    form_set_error('amount', t('Saldo insuficiente.'));
  }
}

function pagarme_marketplace_recipient_transfer_form_submit($form, &$form_state) {
  $amount = $form_state['values']['amount'];
  $recipient_id = $form_state['values']['recipient_id'];
  $bank_account_id = $form_state['values']['bank_account_id'];

  $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();
  $balance = $pagarme_marketplace->pagarme->recipient()->balance(
      new \PagarMe\Sdk\Recipient\Recipient(array('id' => $recipient_id))
  );

  try {
    $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();
    $transfer = $pagarme_marketplace->pagarme->transfer()->create(
        $amount,
        new \PagarMe\Sdk\Recipient\Recipient(array('id' => $recipient_id)),
        new \PagarMe\Sdk\BankAccount\BankAccount(array('id' => $bank_account_id))
    );
    drupal_set_message(t('Saque efetuado com sucesso.'));
  } catch (\Exception $e) {
    watchdog('pagarme_marketplace_error', $e->getMessage(), array(), WATCHDOG_EMERGENCY);
    drupal_set_message(t('Não foi possível efetuar o saque.'), 'error');
  }
}

function pagarme_marketplace_recipient_balance_form($form, &$form_state, $recipient_id) {

  $pagarme_marketplace = new \Drupal\pagarme_marketplace\PagarmeMarketplace();

  $recipient = $pagarme_marketplace->pagarme->recipient()->get($recipient_id);
  $balance = $pagarme_marketplace->pagarme->recipient()->balance(
      $recipient
  );

  $header = array(
    t('Valor a receber'),
    t('Valor disponível'),
    t('Valor já transferido')
  );

  $rows = array();
  $currency_code = commerce_default_currency();
  $currency = commerce_currency_load($currency_code);

  $waiting_funds = $balance->getWaitingFunds()->amount;
  $rows['data']['waiting_funds'] = pagarme_currency_format($waiting_funds);

  $available = $balance->getAvailable()->amount;
  $rows['data']['available'] =  pagarme_currency_format($available);

  $transferred = $balance->getTransferred()->amount;
  $rows['data']['transferred'] =  pagarme_currency_format($transferred);

  $form['recipient']['balance'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Saldo'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  $form['recipient']['balance']['info'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
 
   $form['recipient']['account'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Informações da conta do recebedor'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  $rows = array();
  $rows[] = array(t('NOME/RAZÃO SOCIAL'), $recipient->getBankAccount()->getLegalName());
  $rows[] = array(t('BANCO'), $recipient->getBankAccount()->getBankCode());
  $rows[] = array(t('CPF/CNPJ'), $recipient->getBankAccount()->getDocumentNumber());
  $rows[] = array(t('AGÊNCIA'), $recipient->getBankAccount()->getAgencia());
  $rows[] = array(t('CONTA BANCÁRIA'), $recipient->getBankAccount()->getConta());

  $form['recipient']['account']['info'] = array(
    '#markup' => theme('table', array('rows' => $rows))
  );
  
  return $form;
}