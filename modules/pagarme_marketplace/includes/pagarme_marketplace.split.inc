<?php 

/**
 * @file
 * Split implementation.
 */

/**
 * The split definition.
 *
 * @return array
 */
function _pagarme_marketplace_split_menu_definition() {
  $items = array();

  $base = array(
    'access arguments' => array('administer split'),
    'file' => 'includes/pagarme_marketplace.split.inc',
  );

  $items['admin/commerce/config/marketplace/splits'] = array(
    'title' => t('Regras de divisão'),
    'description' => 'Lista de regras de divisão de produtos.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_split_list'),
  ) + $base;

  $items['admin/commerce/config/marketplace/splits/list'] = array(
    'title' => t('Regras de divisão'),
    'description' => 'Lista de regras de divisão de produtos.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_split_list'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  ) + $base;

  $items['admin/commerce/config/marketplace/splits/config'] = array(
    'title' => t('Configurações'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_split_config_form'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
    'file path' => drupal_get_path('module', 'pagarme_marketplace'),
  ) + $base;

  $items['admin/commerce/config/marketplace/splits/search-product'] = array(
    'title' => t('Adicionar regra'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_split_search_product_form'),
    'type' => MENU_LOCAL_ACTION,
  ) + $base;

  $items['admin/commerce/config/marketplace/splits/add/%'] = array(
    'title' => t('Adicionar regra'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_split_form', 5, 6),
    'type' => MENU_CALLBACK,
  ) + $base;

   $items['admin/commerce/config/marketplace/splits/edit/%/%'] = array(
    'title' => t('Editar regra'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_split_form', 5, 6, 7),
    'type' => MENU_CALLBACK,
  ) + $base;

   $items['admin/commerce/config/marketplace/splits/delete/%'] = array(
    'title' => t('Deletar split'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagarme_marketplace_split_delete_form', 6),
    'type' => MENU_CALLBACK,
  ) + $base;

  return $items;
}

function pagarme_marketplace_split_list() {
  $num_per_page = 10;

  $query = db_select('pagarme_splits', 'splits')->extend('PagerDefault');
  $query->leftJoin('commerce_product', 'product', 'product.product_id = splits.product_id');
  $query->fields('splits');
  $query->addField('product', 'title', 'product_title');
  $query->addField('product', 'sku', 'product_sku');
  $query->limit($num_per_page);
  $query->orderBy('changed', 'DESC');
  $splits = $query->execute()->fetchAll();

  $rows = array();
  $destination = drupal_get_destination();
  foreach ($splits as $key => $item) {
    $row = array();
    $row['product_title'] = $item->product_title;
    $row['product_sku'] = $item->product_sku;
    $row['split_type'] = ($item->split_type == 'percentage') ? t('Em percentual') : t('Em centavos');
    $row['status'] = (empty($item->status)) ? t('Inativa') : t('Ativa');

    $operations = array();
    $operations['edit'] = array(
      'title' => t('Editar'),
      'href' => "admin/commerce/config/marketplace/splits/edit/" . $item->product_id . '/' . $item->split_id,
      'query' => $destination,
    );

    $operations['delete'] = array(
      'title' => t('Deletar'),
      'href' => "admin/commerce/config/marketplace/splits/delete/" . $item->split_id,
      'query' => $destination,
    );

    $row['operations'] = array(
      'data' => array(
        '#theme' => 'links',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline', 'nowrap')),
      ),
    );

    $rows[] = $row;
  }

  $build = array();
  $header = array(
    t('Produto'),
    t('SKU'),
    t('Tipo de regra'),
    t('Status'),
    t('Operações')
  );
  $build['recipients_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('Não existem regras de divisão cadastradas.'),
  );
  $build['path_pager'] = array('#theme' => 'pager');

  return $build;
}

function pagarme_marketplace_split_search_product_form($form, &$form_state) {
  $form = array();
  $form = array('#id' => 'pagarme_marketplace_split_search_product_form');

  $form['product'] = array(
    '#type' => 'textfield',
    '#title' => t('Produto'),
    '#description' => t('Use esse campo para encontra e adicionar o produto em que a regra será aplicada.'),
    '#autocomplete_path' => 'commerce_product/autocomplete/commerce_line_item/commerce_product/product',
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continuar'),
    '#submit' => array('pagarme_marketplace_split_search_product_form_submit')
  );

  return $form;
}

function pagarme_marketplace_split_search_product_form_validate($form, &$form_state) {
  if ($product = commerce_product_load_by_sku($form_state['input']['product'])) {
    $result = db_select('pagarme_splits')
      ->fields('pagarme_splits')
      ->condition('product_id', $product->product_id)
      ->execute();
    if ($split = $result->fetchObject()) {
      $path = 'admin/commerce/config/marketplace/splits/edit/' . $split->product_id . '/' . $split->split_id;
      $link = l(t('clique aqui'), $path);
      $message = t('Já existe uma regra de divisão para o produto informado, para editar ' . $link);
      form_error($form['product'], filter_xss_admin($message));
    } 
    else {
      $form_state['values']['product_id'] = $product->product_id;
    }
  } 
  else {
    form_error($form['product'], t('Produto informado não é válido, use o autompletar para adicionar um produto válido'));
  }
}

function pagarme_marketplace_split_search_product_form_submit($form, &$form_state) {
  $product_id = $form_state['values']['product_id'];
  drupal_goto('admin/commerce/config/marketplace/splits/add/' . $product_id);
}

function pagarme_marketplace_split_form($form, &$form_state, $op = 'add', $product_id =  NULL, $split_id =  NULL) {

  // Validação para garantir que o produto realmente exista
  if (!$product = commerce_product_load($product_id)) {
     drupal_goto('admin/commerce/config/marketplace/splits/search-product');
  }

  $default_currency = commerce_default_currency();

  // Validação para impedir o cadastro de mais de uma regra de divisão para um produto
  if ($op == 'add') {
    $split = db_select('pagarme_splits', 'splits')
      ->fields('splits')
      ->condition('product_id', $product_id)
      ->execute()->fetchObject();
    if ($split) {
      $path = 'admin/commerce/config/marketplace/splits/edit/' . $split->product_id . '/' . $split->split_id;
      $link = l(t('clique aqui'), $path);
      $message = t('Já existe uma regra de divisão para o produto informado, para editar ' . $link);
      drupal_set_message(filter_xss_admin($message), 'warning');
      drupal_goto('admin/commerce/config/marketplace/splits/search-product');
    }
  }

  $pagarme_api_key = variable_get('pagarme_api_key');
  $pagarme_drupal = new \Drupal\pagarme\PagarmeDrupal($pagarme_api_key);
  $company_info = $pagarme_drupal->getCompanyInfo();

  $split = db_select('pagarme_splits', 'splits')
    ->fields('splits')
    ->condition('split_id', $split_id)
    ->execute()->fetchObject();

  $form = array();
  $form = array('#id' => 'pagarme_marketplace_split_form');

  $form['split_id'] = array(
    '#type' => 'hidden',
    '#value' => $split_id
  );

  $form['product_id'] = array(
    '#type' => 'hidden',
    '#value' => $product_id
  );

  $title_output = field_view_field('commerce_product', $product, 'title_field', array('label' => 'hidden'));
  $price_output = field_view_field('commerce_product', $product, 'commerce_price', array('label' => 'hidden'));
  $product_output = render($title_output) . render($price_output); 

  $form['product_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Produto vinculado a regra'),
  );

  $form['product_fieldset']['output'] = array(
    '#type' => 'markup',
    '#markup' => $product_output
  );

  $status = (isset($split->status) && empty($split->status)) ? FALSE: TRUE;
  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ativa'),
    '#description' => t('Desmarque essa opção para desativar a regra, ou seja, a regra não será aplicada ao produto no calculo de divisão'),
    '#default_value' => $status,
  );

  $form['split_type'] = array(
    '#type' => 'select',
    '#title' => t('Tipo de divisão'),
    '#description' => t('Selecione o tipo de divisão.'),
    '#options' => array('amount' => 'Em centavos', 'percentage' => 'Em percentual'),
    '#default_value' => (!empty($split->split_type)) 
    ? $split->split_type : ''
  );

  $query = db_select('pagarme_split_rules', 'rules');
  $query->fields('rules');
  $query->addField('recipients', 'legal_name');
  $query->addExpression("CONCAT(recipients.legal_name, ' [', recipients.recipient_id, ']')", 'recipient');
  $query->leftJoin('pagarme_recipients', 'recipients', 'recipients.recipient_id = rules.recipient_id');
  $query->condition('split_id', $split_id);
  $split_rules = $query->execute();

  $rules = array();
  foreach ($split_rules as $key => $item) {
    $rules[] = (array) $item;
  }

  if (!empty($form_state['input']['split_rules'])) {
    $rules = $form_state['input']['split_rules'];
  }

  if (empty($form_state['storage']['rules'])) {
    $form_state['storage']['rules'] = count($rules);
  }

  if (empty($form_state['storage']['remove'])) {
    $form_state['storage']['remove'] = array();
  }

  $form['default_recipient'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Recebedor principal'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  $form['default_recipient']['info'] = array(
    '#type' => 'markup', 
    '#markup' => '<p>RAZÃO SOCIAL: ' . $company_info->name . '<p>',
  );
  
  $default_amount = (!empty($split->amount)) ? $split->amount : 0;
  $default_amount = commerce_currency_amount_to_decimal($default_amount, $default_currency);
  $description = t('Valor ou porcentagem que o recebedor principal vai receber da transação.<br>Exemplos:<br>Valor a receber de R$120,15 = 120.15<br>Valor a receber de R$1.000,25 = 1000.25<br>Percentual a receber de 35% = 35<br>Percentual a receber de 51.99% = 51.99');
  $form['default_recipient']['default_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Valor'),
    '#description' => filter_xss_admin($description),
    '#size' => 20,
    '#default_value' => $default_amount,
    '#element_validate' => array('element_validate_number'),
    '#attributes' => array('class' => array('pagarme-price-format'))
  );

  $form['default_recipient']['default_liable'] = array(
    '#type' => 'checkbox',
    '#title' => 'Chargeback',
    '#default_value' => (empty($split->liable)) ? FALSE : TRUE,
    '#description' => "Define se irá se responsabilizar pelo risco da transação (chargeback)",
  );

  $form['default_recipient']['default_charge_processing_fee'] = array(
    '#type' => 'checkbox',
    '#title' => 'Taxa Pagar.me',
    '#description' => 'Define se irá ser cobrado pela taxa da Pagar.me',
    '#default_value' => (empty($split->charge_processing_fee)) ? FALSE : TRUE,
  );

  $form['split_rules'] = array(
    '#type' => 'markup',
    '#markup' => '',
    '#prefix' => '<table id="rules-fieldset-wrapper">',
    '#suffix' => '</table>',
    '#tree' => TRUE,
  );

  $form['split_rules']['header'] = array(
    '#markup' => '<thead><tr><th>' . t('Recebedor') . '</th><th>' . t('Valor') . '</th><th>' . t('Chargeback') . '</th><th>' . t('Taxa Pagar.me') . '</th><th>' . t('Remover') . '</th></tr></thead>',
  );

  for ($i = 0; $i < $form_state['storage']['rules']; $i++) {
    $style = (in_array($i, $form_state['storage']['remove'])) ? 'style="display:none"' : '';
    $form['split_rules'][$i] = array(
      '#prefix' => '<tr ' . $style . '>',
      '#suffix' => '</tr>',
      '#tree' => TRUE,
    );

    $form['split_rules'][$i]['recipient'] = array(
      '#type' => 'textfield',
      '#description' => t('Recebedor que irá receber o valor descrito nessa regra.'),
      '#autocomplete_path' => 'pargame-marketplace/autocomplete/recipient',
      '#default_value' => (!empty($rules[$i]['recipient'])) ? $rules[$i]['recipient'] : '',
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );

    $rule_amount = commerce_currency_amount_to_decimal($rules[$i]['amount'], $default_currency);
    $form['split_rules'][$i]['amount'] = array(
      '#type' => 'textfield',
      '#title' => 'Valor',
      '#title_display' => 'invisible',
      '#description' => t('Valor ou porcentagem que o recebedor vai receber da transação.'),
      '#default_value' => $rule_amount,
      '#element_validate' => array('element_validate_number'),
      '#size' => 20,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#attributes' => array('class' => array('pagarme-price-format'))
    );

    $form['split_rules'][$i]['liable'] = array(
      '#type' => 'checkbox',
      '#description' => "Define se o recebedor irá se responsabilizar pelo risco da transação (chargeback)",
      '#default_value' => (!empty($rules[$i]['liable'])) ? TRUE : FALSE,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );

    $form['split_rules'][$i]['charge_processing_fee'] = array(
      '#type' => 'checkbox',
      '#description' => 'Define se o recebedor irá ser cobrado pela taxa da Pagar.me',
      '#default_value' => (!empty($rules[$i]['charge_processing_fee'])) ? TRUE : FALSE,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );

    $form['split_rules'][$i]['remove'] = array(
      '#type' => 'checkbox',
      '#default_value' => (!empty($rules[$i]['remove'])) ? $rules[$i]['remove'] : '',
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );
  }

  $form['split_rules'][] = array(
    '#prefix' => '<tr>',
    '#suffix' => '</tr>',
    'add_split_rule' => array(
      '#type' => 'submit',
      '#value' => t('Adicionar nova regra'),
      '#ajax' => array(
        'callback' => '_pagarme_marketplace_split_rule_ajax_callback',
        'wrapper' => 'rules-fieldset-wrapper',
      ),
      '#prefix' => '<td colspan="4">',
      '#suffix' => '</td>',
      '#limit_validation_errors' => array(),
      '#submit' => array('_pagarme_marketplace_split_rule_add_one'),
    ),
    'line_remove' => array(
      '#type' => 'submit',
      '#value' => t('Remover'),
      '#ajax' => array(
        'callback' => '_pagarme_marketplace_split_rule_ajax_callback',
        'wrapper' => 'rules-fieldset-wrapper',
      ),
      '#prefix' => '<td>',
      '#suffix' => '</td>',
      '#limit_validation_errors' => array(),
      '#submit' => array('_pagarme_marketplace_split_rule_remove_one'),
    )
  );

  if ($op == 'edit' && !empty($spid)) {
    $query = db_select('pagarme_splits', 'pms');
    $query->fields('pms');
    $query->leftJoin('pagarme_recipients', 'pmr', 'pmr.recipient_id = pms.recipient_id');
    $query->addField('pmr', 'legal_name');
    $query->condition('pms', $spid);
    $split = $query->execute()->fetchObject();

    $form['product']['#default_value'] = $split->product_sku;
    $form['recipient']['#default_value'] = $split->legal_name . ' [' . $split->recipient_id . ']';
    $form['type']['#default_value'] = $split->type;
    $form['percentage']['#default_value'] = $split->percentage;
    $form['amount']['#default_value'] = $split->amount;
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Salvar regra'),
    '#submit' => array('pagarme_marketplace_split_form_submit'),
    '#validate' => array('pagarme_marketplace_split_form_validate')
  );
  return $form;
}

function _pagarme_marketplace_split_rule_ajax_callback($form, &$form_state) {
  return $form['split_rules'];
}

function _pagarme_marketplace_split_rule_add_one($form, &$form_state) {
  $form_state['storage']['rules']++;
  $form_state['rebuild'] = TRUE;
}

function _pagarme_marketplace_split_rule_remove_one($form, &$form_state) {
  foreach ($form_state['input']['split_rules'] as $key => $rule) {
    if (!empty($rule['remove'])) {
      $form_state['storage']['remove'][$key] = $key;
    }
  }
  // foreach ($form_state['input']['split_rules'] as $rule) {
  //   if (!empty($rule['remove'])) {
  //     $form_state['storage']['rules']--;
  //   }
  // }
  // if ($form_state['storage']['rules'] < 0) {
  //   $form_state['storage']['rules'] = 0;
  // }
  $form_state['rebuild'] = TRUE;
}

function pagarme_marketplace_split_form_validate($form, &$form_state) {
  $total_rule = 0;
  $default_currency = commerce_default_currency();

  $has_liable = FALSE;
  $has_charge_processing_fee = FALSE;
  if (!empty($form_state['input']['default_liable'])) {
    $has_liable = TRUE;
  }
  if (!empty($form_state['input']['default_charge_processing_fee'])) {
    $has_charge_processing_fee = TRUE;
  }

  $default_amount = &$form_state['input']['default_amount'];
  $default_amount = commerce_currency_decimal_to_amount($default_amount, $default_currency);
  $total_rule += $default_amount;

  if (!empty($form_state['input']['split_rules'])) {
    foreach ($form_state['input']['split_rules'] as $key => &$rule) {
      if (in_array($key, $form_state['storage']['remove'])) {
        continue;
      }
      // validação de recebedor
      if (empty($rule['recipient'])) {
        $message = t('Recebedor, campo é obrigatório.');
        form_error($form['split_rules'][$key]['recipient'], $message);
      } 
      else {
        $matches = array();
        $result = preg_match('/\[([0-9]+)\]$/', $rule['recipient'], $matches);
        if ($result > 0) {
          $id = $matches[$result];
          $query = db_select('pagarme_recipients', 'recipients')
            ->fields('recipients', array('recipient_id'))
            ->condition('recipient_id', $id);
          if ($query->execute()->rowCount()) {
            $rule['recipient_id'] = $id;
          } 
          else {
            $message = t('Recebedor com id %id não pode ser encontrado', array('%id' => $id));
            form_error($form['split_rules'][$key]['recipient'], $message);
          }
        } 
        else {
            $message = t('Recebedor informado não é válido, use o autompletar para adicionar um recebedor válido');
            form_error($form['split_rules'][$key]['recipient'], $message);
        }
      }

      $rule['amount_integer'] = commerce_currency_decimal_to_amount($rule['amount'], $default_currency);
      $total_rule += $rule['amount_integer'];

      if (!empty($rule['liable'])) {
        $has_liable = TRUE;
      }
      if (!empty($rule['charge_processing_fee'])) {
        $has_charge_processing_fee = TRUE;
      }
    }
  }

  if (!$has_liable) {
    form_error($form['default_recipient']['default_liable'], t('É necessário informar no mínimo um recebedor que será responsável pelo risco da transação (chargeback).'));
  }

  if (!$has_charge_processing_fee) {
    form_error($form['default_recipient']['default_charge_processing_fee'], t('É necessário informar no mínimo um recebedor que será cobrado pela taxa do Pagar.me'));
  }

  switch ($form_state['input']['split_type']) {
    case 'percentage':
      if ($total_rule !== 10000) {
        form_error($form['split_rules'], t('A soma da regra de divisão deve ser de 100%'));
      }
      break;
    default:
      $product = commerce_product_load($form_state['input']['product_id']);
      $commerce_product = entity_metadata_wrapper('commerce_product', $product);
      $price = $commerce_product->commerce_price->amount->value();

      if ($total_rule != $price) {
        $price = pagarme_currency_format($price);
        form_error($form['split_rules'], filter_xss_admin(t("A soma da regra de divisão deve ser igual ao valor do produto $price")));
      }
      break;
  }
}

function pagarme_marketplace_split_form_submit($form, &$form_state) {
  $default_amount = 0;
  if (!empty($form_state['input']['default_amount'])) {
    $default_amount = $form_state['input']['default_amount'];
  }
  $default_liable = 0;
  if (!empty($form_state['input']['default_liable'])) {
    $default_liable = $form_state['input']['default_liable'];
  }
  $default_charge_processing_fee = 0;
  if (!empty($form_state['input']['default_charge_processing_fee'])) {
    $default_charge_processing_fee = $form_state['input']['default_charge_processing_fee'];
  }
  $split = array(
    'split_id' => $form_state['input']['split_id'],
    'product_id' => $form_state['input']['product_id'],
    'split_type' => $form_state['input']['split_type'],
    'amount' => $default_amount,
    'liable' => $default_liable,
    'charge_processing_fee' => $default_charge_processing_fee,
    'status' => (empty($form_state['input']['status'])) ?  0 : 1,
  );

  if (!empty($form_state['input']['split_rules'])) {
    foreach ($form_state['input']['split_rules'] as $key => $item) {
      if (in_array($key, $form_state['storage']['remove'])) {
        continue;
      }
      $amount = (!empty($item['amount_integer'])) ? $item['amount_integer'] : 0;
      $liable = 0;
      if (!empty($item['liable'])) {
        $liable = $item['liable'];
      }
      $charge_processing_fee = 0;
      if (!empty($item['charge_processing_fee'])) {
        $charge_processing_fee = $item['charge_processing_fee'];
      }
      $rule = array();
      $rule['split_id'] = $form_state['input']['split_id'];
      $rule['recipient_id'] = $item['recipient_id'];
      $rule['amount'] = $amount;
      $rule['liable'] = $liable;
      $rule['charge_processing_fee'] = $charge_processing_fee;
      $split['rules'][] = $rule;
    }
  }
  pagarme_marketplace_split_save($split);
  drupal_set_message(t('Regra de divisão salva.'));
  drupal_goto('admin/commerce/config/marketplace/splits');
}

function pagarme_marketplace_cancel() {
  return FALSE;
}

function pagarme_marketplace_split_delete_form($form, &$form_state, $split_id) {
  $form = array();
  $form = array('#id' => 'pagarme_marketplace_split_delete_form');

  $form['split_id'] = array(
    '#type' => 'hidden',
    '#value' => $split_id,
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Deletar'),
    '#submit' => array('pagarme_marketplace_split_delete'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('pagarme_marketplace_cancel'),
  );

  return $form;
}

function pagarme_marketplace_split_delete($form, &$form_state) {
  db_delete('pagarme_splits')
    ->condition('split_id', $form_state['values']['split_id'])
    ->execute();
  db_delete('pagarme_split_rules')
    ->condition('split_id', $form_state['values']['split_id'])
    ->execute();
  drupal_set_message(t('Regra de divisão deletada.'));
}


/**
 * Builds the split settings form.
 */
function pagarme_marketplace_split_config_form() {
  $types = commerce_line_item_type_get_name();
  $form['pagarme_marketplace_split_line_item_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Tipos de item de fatura para utilizar split'),
    '#default_value' => variable_get('pagarme_marketplace_split_line_item_types', array()),
    '#options' => $types,
    '#description' => t('Selecione os tipos de itens de pedido que serão considerados no calculo de split.'),
  );
  return system_settings_form($form);
}
