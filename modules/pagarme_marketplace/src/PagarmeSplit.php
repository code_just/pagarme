<?php

namespace Drupal\pagarme_marketplace;

class PagarmeSplit {

  static public function load($split_id, $load_rules = false) {
    $split = db_select('pagarme_splits', 'splits')
      ->fields('splits')
      ->condition('split_id', $split_id)
      ->execute()
      ->fetchObject();

    if ($split && $load_rules) {
      $split->rules = db_select('pagarme_split_rules', 'rules')
        ->fields('rules')
        ->condition('split_id', $split->split_id)
        ->execute()
        ->fetchAll();
    }
    return $split;
  }

  static public function loadByProductId($product_id, $status = PAGARME_SPLIT_ACTIVE, $load_rules = TRUE) {
    $split = db_select('pagarme_splits', 'splits')
      ->fields('splits')
      ->condition('product_id', $product_id)
      ->condition('status', $status)
      ->execute()
      ->fetchObject();

    if ($split && $load_rules) {
      $query = db_select('pagarme_split_rules', 'rules');
      $query->fields('rules');
      $query->addField('recipients', 'pagarme_id', 'recipient_pagarme_id');
      $query->leftJoin('pagarme_recipients', 'recipients', 'recipients.recipient_id = rules.recipient_id');
      $query->condition('split_id', $split->split_id);
      $split->rules = $query->execute()->fetchAll();
    }
    return $split;
  }
}
