<?php

namespace Drupal\pagarme_marketplace;

use Drupal\pagarme\PagarmeSdk;

class PagarmeMarketplace extends PagarmeSdk {

  public function __construct() {
    $api_key = variable_get('pagarme_api_key');
    parent::__construct($api_key);
  }
}
