<?php

namespace Drupal\pagarme_marketplace;

use Drupal\pagarme_marketplace\PagarmeMarketplace;
use PagarMe\Sdk\BankAccount\BankAccount;
use PagarMe\Sdk\Recipient\Recipient;

class PagarmeRecipient extends PagarmeMarketplace {

  public function create($data) {
    $bank_account = new BankAccount($data['bankAccount']);
    return $this->pagarme->recipient()->create(
        $bank_account,
        $data['transferInterval'],
        $data['transferDay'],
        $data['transferEnabled'],
        FALSE,
        0
    );
  }

  public function update($data) {
    $recipient = new Recipient(array(
        'id' => $data['id'],
        'bankAccount' =>  new BankAccount($data['bankAccount']),
        'transferInterval' => $data['transferInterval'],
        'transferDay' => $data['transferDay'],
        'transferEnabled' => $data['transferEnabled'],
    ));
    return $this->pagarme->recipient()->update($recipient);
  }

  public function refreshRecipientsTable() {
    $recipients = $this->pagarme->recipient()->getList(1, 20);
    foreach ($recipients as $recipient) {
      $fields = array(
        'pagarme_id' => $recipient->getId(),
        'transfer_enabled' => (int) $recipient->getTransferEnabled(),
        'transfer_interval' => $recipient->getTransferInterval(),
        'transfer_day' => (int) $recipient->getTransferDay(),
        'bank_id' => $recipient->getBankAccount()->getId(),
        'bank_code' => $recipient->getBankAccount()->getBankCode(),
        // 'type' => $recipient->getBankAccount()->getType(),
        'type' => '',
        'legal_name' => $recipient->getBankAccount()->getLegalName(),
        'document_number' => $recipient->getBankAccount()->getDocumentNumber(),
        'agencia' => $recipient->getBankAccount()->getAgencia(),
        'agencia_dv' => $recipient->getBankAccount()->getAgenciaDv(),
        'conta' => $recipient->getBankAccount()->getConta(),
        'conta_dv' => $recipient->getBankAccount()->getContaDv(),
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
        'archived' => 0,
      );
      db_merge('pagarme_recipients')
        ->key(array('pagarme_id' => $recipient->getId()))
        ->fields($fields)
        ->execute();
    }
  }
}
