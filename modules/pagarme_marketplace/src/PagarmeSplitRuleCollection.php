<?php

namespace Drupal\pagarme_marketplace;

use Drupal\pagarme\PagarmeSdk as PagarmeSdk;

use PagarMe\Sdk\Recipient\Recipient;
use PagarMe\Sdk\SplitRule\SplitRuleCollection;

class PagarmeSplitRuleCollection extends PagarmeSdk {

  protected $order;
  protected $order_wrapper;
  protected $currency_code;

  protected $payment_method;
  protected $final_amount;

  protected $billet_per_discount = 0;
  protected $credit_card_per_interest = 0;

  protected $apply_split_rule = FALSE;
  protected $split_rule_remnant = 0;

  public function __construct($order, $payment_method, $final_amount) {
    $api_key = variable_get('pagarme_api_key');
    parent::__construct($api_key);

    $this->currency_code = commerce_default_currency();

    $this->order = $order;
    $this->order_wrapper = entity_metadata_wrapper('commerce_order', $this->order);

    $this->payment_method = $payment_method;
    $this->final_amount = $final_amount;
    switch ($this->payment_method) {
      case 'credit_card':
        $this->creditCardPercentageInterest();
        break;
      case 'boleto':
        $this->billetPercentageDiscount();
        break;
    }
  }

  public function doSplitRuleCollection() {
    $rules = array();
    $split_rule_mapping = $this->splitRuleMapping();

    $total_amount_final_rule = 0;
    if ($this->apply_split_rule) {
      $split_rule_collection = new SplitRuleCollection();
      foreach ($split_rule_mapping as $recipient_id => $items) {
        foreach ($items as $rule) {
          $amount = $rule['amount'];
          // Os centavos que não foram possível distribuir em uma divisão exata será distribuida entres os recebedores até atingir o valor total da sobra.
          if ($this->split_rule_remnant > 0) {
            $amount++;
            $this->split_rule_remnant--;
          }
          $total_amount_final_rule += $amount;
          /** @var $splitRule PagarMe\Sdk\SplitRule\SplitRule */
          $split_rule_collection[] = $this->pagarme->splitRule()->monetaryRule(
              $amount,
              new Recipient(array('id' => $recipient_id)),
              $rule['liable'],
              $rule['charge_processing_fee']
          );
        }
      }

      if ((int) $total_amount_final_rule !== (int) $this->final_amount) {
        $error_msg = print_r($split_rule_mapping, TRUE)
          . print_r(array('pagarme_final_amount' => $this->final_amount), TRUE);
        throw new \ErrorException('pagarme_split_rules - invalid split rule: <pre>' . $error_msg . '</pre>');
      }
      $rules['splitRules'] = $split_rule_collection;
    }

    if (variable_get('pagarme_debug', FALSE)) {
      watchdog('pagarme_debug', t('@split_rule_mapping: <pre>@pre</pre>'), array('@pre' => print_r($split_rule_mapping, TRUE)), WATCHDOG_DEBUG);
      watchdog('pagarme_debug', t('@split_rule_collection: <pre>@pre</pre>'), array('@pre' => print_r($rules, TRUE)), WATCHDOG_DEBUG);
    }
    return $rules;
  }

  protected function creditCardPercentageInterest() {
    // Valor do pedido sem o desconto
    $order_amount = $this->order_wrapper->commerce_order_total->amount->value();

    // Valor final do pedido (com/sem juros)
    $final_amount = $this->final_amount;

    // Validando juros aplicados ao valor do pedido
    if ((int) $final_amount > (int) $order_amount) {

      // Diferenção do valor do pedido e o valor final
      $amount_interest = $final_amount - $order_amount;

      // Percentual de juros no valor final
      $this->credit_card_per_interest = $amount_interest * 100 / $order_amount;
    }
    return $this->credit_card_per_interest;
  }

  protected function billetPercentageDiscount() {
    // Valor do pedido sem o desconto
    $order_amount = $this->order_wrapper->commerce_order_total->amount->value();

    // Valor final do pedido (com/sem desconto)
    $final_amount = $this->final_amount;

    // $billet_percentage_discount = 0;

    // Validando se o valor do boleto tem desconto
    if ((int) $final_amount < (int) $order_amount) {

      // Valor do desconto aplicado
      $billet_amount_difference = $order_amount - $final_amount;

      $discount_amount = 0;

      // Desconto boleto (Percentual/Valor em centavos)
      switch (variable_get('pagarme_boleto_discount', null)) {
        case 'amount':
          $discount_amount = variable_get('pagarme_boleto_discount_amount', null);
          break;
        case 'percentage':
          $discount_percentage = variable_get('pagarme_boleto_discount_percentage', null);
          if ($discount_percentage) {
            // Valor do desconto a ser aplicado
            $discount_amount = $order_amount * $discount_percentage / 100;
          }
          break;
        default:
          throw new \ErrorException('Order '.$this->order->order_id.', there is a divergence in the value of the order and the value processed by the Pagar.me.');
      }
      $this->billet_per_discount = $discount_amount * 100 / $order_amount;
    }
    return $this->billet_per_discount;
  }

  public function splitRuleMapping() {
    $split_rule_mapping = array();

    $company_info = $this->getCompanyInfo();

    // Default Recipient ID Pagarme.me
    $server = $this->pagarme_config['pagarme_server'];
    $default_recipient_id = $company_info->default_recipient_id->{$server};

    $total_amount_rule = 0;

    $split_line_item_types = array_filter(variable_get('pagarme_marketplace_split_line_item_types', array()));

    // Iterating cart items
    foreach ($this->order_wrapper->commerce_line_items->value() as $line_item) {
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

      if (in_array($line_item_wrapper->type->value(), $split_line_item_types)) {

        $product_id = $line_item_wrapper->commerce_product->product_id->value();

        // Split rules registered for the product
        $split = \Drupal\pagarme_marketplace\PagarmeSplit::loadByProductId($product_id);

        if ($split) {
          // Apply split rules if you have at least one product in the cart that has a split rule register
          $this->apply_split_rule = TRUE;
          if ($split->amount > 0 || $split->liable ||  $split->charge_processing_fee) {
            // Calculates the value in cents for the default recipient, over the total value of the item
            $recipient_rule = $this->calculateValueCents(
                $split->split_type,
                $split->amount,
                $line_item_wrapper
            );
            $recipient_rule += array(
              'liable' => $split->liable,
              'charge_processing_fee' => $split->charge_processing_fee
            );
            $split_rule_mapping[$default_recipient_id][] = $recipient_rule;
            $total_amount_rule += $recipient_rule['amount'];
          }
          foreach ($split->rules as $rule) {
            if ($rule->amount > 0 || $rule->liable || $rule->charge_processing_fee) {
              $recipient_pagarme_id = $rule->recipient_pagarme_id;

              // Calculates the value in cents for other recipient, over the total value of the item
              $recipient_rule = $this->calculateValueCents(
                  $split->split_type,
                  $rule->amount,
                  $line_item_wrapper
              );
              $recipient_rule += array(
                'liable' => $rule->liable,
                'charge_processing_fee' => $rule->charge_processing_fee
              );
              $split_rule_mapping[$recipient_pagarme_id][] = $recipient_rule;
              $total_amount_rule += $recipient_rule['amount'];
            }
          }
        }

        // If the product does not have a split rule registered 100% of the value goes to the default recipient
        else {
          // Aplicar desconto ou juros(boleto/cartão) proporcional ao valor a ser recebido
          $amount = $this->balanceAmount($line_item_wrapper->commerce_total->amount->value());

          $split_rule_mapping[$default_recipient_id][] = array(
            'sku' => $line_item_wrapper->commerce_product->sku->value(),
            'amount' => $amount,
            'liable' => TRUE,
            'charge_processing_fee' => TRUE
          );
          $total_amount_rule += $amount;
        }
      }
    }

    // A diferença do valor é os centavos que não é possivel adicionar em uma divisão exata de um número inteiro representando centavos.
    $this->split_rule_remnant = $this->final_amount - $total_amount_rule;
    return $split_rule_mapping;
  }

  protected function calculateValueCents($split_type, $value, $line_item_wrapper) {
    $amount = 0;
    $commerce_total = $line_item_wrapper->commerce_total->amount->value();
    switch ($split_type) {
      case 'percentage':
        // O percentual é armazenado como número inteiro, no mesmo formato que valor, permitindo duas casas decimais
        $value = commerce_currency_amount_to_decimal($value, $this->currency_code);
        $amount = $commerce_total * $value / 100;
        break;
      case 'amount':
        $quantity = (int) $line_item_wrapper->quantity->value();
        $amount = $quantity * $value;
        break;
      default:
        break;
    }

    // Aplicar desconto ou juros(boleto/cartão) proporcional ao valor a ser recebido
    $amount = $this->balanceAmount($amount);
    return array(
      'sku' => $line_item_wrapper->commerce_product->sku->value(),
      'amount' => $amount,
    );
  }

  public function balanceAmount($amount) {
    switch ($this->payment_method) {
      case 'boleto':
        $discount_amount = $amount * $this->billet_per_discount / 100;
        $amount -= $discount_amount;
        break;
      case 'credit_card':
        $interest_amount = $amount * $this->credit_card_per_interest / 100;
        $amount += $interest_amount;
        break;
    }

    // O valor deve ser um número inteiro, pois o valor é um número inteiro representando centavos.
    return floor($amount);
  }
}
