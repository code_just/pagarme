README.txt
==========

Este módulo recriar algumas funcionalidades existentes na dashboard da Pagar.me (https://pagar.me/) no Drupal Commerce.
Você poderá fazer toda a gestão de marketplace.
Para possibilitar o pagamento de seus recebedores, terá a sua disposição:
 - Cadastro de recebedor
 - Saldo de recebedor
 - Saque(Ted) para a conta de um recebedor
 - Criação de splits por produto
 - Estorno de transação


REQUERIMENTOS
=============

*** ENGLISH ***

This module recreates some functionality in the dashboard of Pagar.me (https://pagar.me/) in Drupal Commerce.
You can do all marketplace management.
To enable the payment of your recipients, you will have at your disposal:
 - Recipient registration
 - Recipient balance
 - Cash (Ted) to a recipient's account
 - Creating splits by product
 - Transaction refund

REQUIREMENTS
============
