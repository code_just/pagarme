<?php

namespace Drupal\pagarme;

/**
 * @file Class encapsulating the business logic related to integrate Pagar.me with
 * Drupal structures.
 */
class PagarmeSdk {

  public $pagarme;
  protected $api_key;
  protected $pagarme_config;

  public function __construct($api_key = null) {
    if ($api_key) {
      $this->setup($api_key);
    }
    $this->setPagarmeConfig();
  }

  protected function setPagarmeConfig() {
    $this->pagarme_config = \Drupal\pagarme\Helpers\PagarmeUtility::getPagarmeConfig();
  }

  public function getPagarmeConfig() {
    return $this->pagarme_config;
  }

  public function getCompanyInfo() {
    if($cached = cache_get('pagarme_company_info', 'cache'))  {
      $company_info = $cached->data;
    } else {
      $company_info = $this->pagarme->company()->info();
      cache_set('pagarme_company_info', $company_info, 'cache', 60*60);
    }
    return $company_info;
  }

  /**
   * Setup Pagar.me PHP SDK
   */
  protected function setup($api_key) {
    $this->loadLibrary();
    $this->api_key = $api_key;
    $this->pagarme = new \PagarMe\Sdk\PagarMe($this->api_key);
  }

  /**
   * Returns TRUE if the \PagarMe\Sdk\PagarMe class is available.
   */
  protected function loadLibrary() {

    $pagarme_lib_url = 'https://github.com/pagarme/pagarme-php';
    $path = 'sites/all/libraries/pagarme-php';

    if (file_exists($path)) {
      require_once $path . '/vendor/autoload.php';
    }
    else {
      $message = 'Pagar.me PHP SDK was not found on "' . $path . '". Download it in ' . $pagarme_lib_url;
      watchdog('pagarme_error', t($message), array(), WATCHDOG_ERROR);
      throw new \Exception(t($message));
    }
    return class_exists('\PagarMe\Sdk\PagarMe') ? TRUE : FALSE;
  }
}
