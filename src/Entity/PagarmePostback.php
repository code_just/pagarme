<?php

namespace Drupal\pagarme\Entity;

/**
 * @file
 * Class encapsulating the business logic related to the processing of 
 * notifications data that Pagar.me sends back to Drupal.
 */
class PagarmePostback {

  private $pagarme_data;
  private $order_id;
  private $payment_method = 'pagarme_cp';

  /**
   * base constructor of class, it gets as parameter an $_POST from Pagar.me
   */
  public function __construct($pagarme_data) {
    if (!empty($pagarme_data)) {
      $this->pagarme_data = $pagarme_data;
      if (!empty($this->pagarme_data['order_id'])) {
        $this->order_id = $this->pagarme_data['order_id'];
      }
    }
    else {
      throw new ErrorException('Pagar.me data is null');
    }
  }

  /**
   * 
   * @param type $data $_POST data or array based on it to process a Pagar.me POSTback
   * @return \PagarmePostback
   * @throws Exception
   */
  static public function create($data) {

    if (variable_get('pagarme_debug', FALSE)) {
      watchdog('pagarme_debug', '[processPagarmeData] create <pre>@pre</pre>', array(
        '@pre' => print_r($data, TRUE)
      ));
    }

    // Validating the POSTback source, that is, if it was actually sent by Pagar.me.me
    if (!self::validateRequestSignature()) {
      watchdog(
        'pagarme_error'
        , t('An attempt was made to access the POSTback notification URL from an unknown source. See below: <pre>@pre</pre>', array(
          '@pre' => print_r($_REQUEST, TRUE)
        ))
        , array()
        , WATCHDOG_ERROR
      );
      throw new Exception(t('An attempt was made to access the POSTback notification URL from an unknown source'));
    }
    $postback = (array) $data;
    $postback['pagarme_id'] = $data['id'];
    $postback['payment_status'] = $data['current_status'];

    foreach (module_implements('pagarme_preprocess_postback') as $module) {
      $function = $module . '_pagarme_preprocess_postback';
      $function($postback);
    }
    return new PagarmePostback($postback);
  }

  /**
   * Main controller for processing Pagar.me POSTback data, ensuring database integrity 
   * with a db_transaction and making it easy to extend with another class, in 
   * projects with special use cases
   * @throws Exception
   */
  public function processPagarmeData() {

    $transaction = db_transaction();

    try {
      $this->savePaymentTransaction();

      if (variable_get('pagarme_debug', FALSE)) {
        watchdog('pagarme_debug', '[processPagarmeData] updated <pre>@pre</pre>', array(
          '@pre' => print_r($this, TRUE)
        ));
      }

      $this->updateOrderStatus();
      $this->save();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('pagarme_error', $e);
      throw $e;
    }
  }

  public function savePaymentTransaction() {

    // Check if this is an update to an existing transaction or if we need to 
    // create one
    $previous_postback_data = $this->load($this->pagarme_data['pagarme_id']);

    if ($previous_postback_data) {
      $this->order_id = $previous_postback_data['order_id'];

      // Load the prior POSTback transaction and update that with the capture values.
      $transaction = commerce_payment_transaction_load($previous_postback_data['transaction_id']);
    }
    else {
      // Create a new payment transaction for the order.
      $transaction = commerce_payment_transaction_new($this->payment_method, $this->order_id);
      $transaction->instance_id = $this->payment_method;
      $transaction->amount = $this->pagarme_data['amount'];
    }

    $transaction->remote_id = $this->pagarme_data['pagarme_id'];

    // Pagar.me supports only Brazilian Reais.
    $transaction->currency_code = 'BRL';

    $transaction->payload[REQUEST_TIME] = $this->pagarme_data;

    // Set the transaction's statuses based on the POSTback payment_status.
    $transaction->remote_status = $this->pagarme_data['payment_status'];

    switch ($this->pagarme_data['payment_status']) {
      case PAGARME_STATUS_PROCESSING:
        $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
        break;
      case PAGARME_STATUS_AUTHORIZED:
        $transaction->status = PAGARME_STATUS_AUTHORIZED;
        break;
      case PAGARME_STATUS_PAID:
        $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
        break;
      case PAGARME_STATUS_WAITING_PAYMENT:
        $transaction->status = PAGARME_STATUS_WAITING_PAYMENT;
        break;
      case PAGARME_STATUS_REFUNDED:
        $transaction->status = PAGARME_STATUS_REFUNDED;
        break;
      case PAGARME_STATUS_PENDING_REFUND:
        $transaction->status = PAGARME_STATUS_PENDING_REFUND;
        break;
      case PAGARME_STATUS_REFUSED:
        $transaction->status = PAGARME_STATUS_REFUSED;
        break;
      case PAGARME_STATUS_CHARGEDBACK:
        $transaction->status = PAGARME_STATUS_CHARGEDBACK;
        break;
    }

    $transaction->message = $this->getStatusDescription($this->pagarme_data['payment_status']);
    $transaction->message_variables = array();

    commerce_payment_transaction_save($transaction);
    $this->pagarme_data['transaction_id'] = $transaction->transaction_id;

    $this->transaction = $transaction;

    if (variable_get('pagarme_debug', FALSE)) {
      watchdog('pagarme_debug', '[savePaymentTransaction] processed for order @order_number, with ID @pagarme_id, changing it status to @status <pre>@pre</pre>', array(
        '@pagarme_id' => $this->pagarme_data['pagarme_id']
        , '@order_number' => $this->order_id
        , '@status' => self::getCorrespondingStatus($this->pagarme_data['payment_status']
        )), WATCHDOG_INFO);
    }
  }

  /**
   * Update drupal order status, according to data from POSTback, doing the 
   * correlation with Pagar.me and Drupal Commerce statuses.
   */
  public function updateOrderStatus() {
    $order = commerce_order_load($this->order_id);
    commerce_order_status_update(
        $order, self::getCorrespondingStatus($this->pagarme_data['payment_status'])
        , FALSE, NULL, $this->getStatusDescription($this->pagarme_data['payment_status'])
    );
  }

  /**
   * Persist data from Pagar.me POSTback to the database
   * @return boolean
   */
  public function save() {

    $previous_postback_data = $this->load($this->pagarme_data['pagarme_id']);

    if (!empty($this->pagarme_data['pagarme_id']) && $previous_postback_data) {

      $this->pagarme_data['ppid'] = $previous_postback_data['ppid'];
      $this->pagarme_data['changed'] = REQUEST_TIME;
      return drupal_write_record('pagarme_postback', $this->pagarme_data, 'ppid');
    }
    else {

      $this->pagarme_data['created'] = REQUEST_TIME;
      $this->pagarme_data['changed'] = REQUEST_TIME;

      return drupal_write_record('pagarme_postback', $this->pagarme_data);
    }
  }

  /**
   * Return a list of payment statuses, or an specific payment status message.
   *
   * @param $status
   *   The status identification in which to return the message.
   */
  public function getStatusDescription($status = NULL) {
    $statuses = array(
      PAGARME_STATUS_PROCESSING => t('Transação sendo processada.'),
      PAGARME_STATUS_AUTHORIZED => t('Transação autorizada. Cliente possui saldo na conta e este valor foi reservado para futura captura, que deve acontecer em no máximo 5 dias. Caso a transação não seja capturada, a autorização é cancelada automaticamente.'),
      PAGARME_STATUS_PAID => t('Transação paga (autorizada e capturada).'),
      PAGARME_STATUS_WAITING_PAYMENT => t('Transação aguardando pagamento (transação realizada com boleto bancário).'),
      PAGARME_STATUS_REFUNDED => t('Transação estornada.'),
      PAGARME_STATUS_PENDING_REFUND => t('Transação paga com boleto aguardando para ser estornada.'),
      PAGARME_STATUS_REFUSED => t('Transação não autorizada.'),
      PAGARME_STATUS_CHARGEDBACK => t('Transação sofreu chargeback.'),
    );
    return empty($status) ? $statuses : $statuses[$status];
  }

  /**
   * Returns the corresponding status from Drupal Commerce, for the status used in
   * Pagar.me
   *
   * @param int $remote_status
   *   The status from Pagar.me
   * @return string
   */
  public static function getCorrespondingStatus($remote_status) {
    $statuses = array(
      PAGARME_STATUS_PROCESSING => 'pending',
      PAGARME_STATUS_AUTHORIZED => PAGARME_STATUS_AUTHORIZED,
      PAGARME_STATUS_PAID => 'completed',
      PAGARME_STATUS_WAITING_PAYMENT => PAGARME_STATUS_WAITING_PAYMENT,
      PAGARME_STATUS_REFUNDED => PAGARME_STATUS_REFUNDED,
      PAGARME_STATUS_PENDING_REFUND => PAGARME_STATUS_PENDING_REFUND,
      PAGARME_STATUS_REFUSED => PAGARME_STATUS_REFUSED,
      PAGARME_STATUS_CHARGEDBACK => PAGARME_STATUS_CHARGEDBACK
    );
    if (!in_array($remote_status, array_keys($statuses))) {
      throw new ErrorException(t('The payment status sent by Pagar.me was not recognized by pagarme module. Please file an issue at http://drupal.org/project/pagarme.'));
    }
    return $statuses[$remote_status];
  }

  /**
   * Load a Pagar.me POSTback data from pagarme_postback table based on the pagarme_id
   */
  public function load($pagarme_id) {
    $query = db_select('pagarme_postback')
        ->fields('pagarme_postback')
        ->condition('pagarme_id', $pagarme_id)
        ->execute()
        ->fetchAssoc();
    return empty($query) ? FALSE : $query;
  }

  /**
   * Load the last a Pagar.me POSTback data from pagarme_postback table based on the pagarme_id
   */
  public function loadLastPagarmeId() {
    $result = db_query('select max(pagarme_id) as pagarme_id from {pagarme_postback} where order_id = :order_id', array(':order_id' => $this->order_id))->fetchObject();
    return empty($result) ? FALSE : $result->pagarme_id;
  }

  static private function validateRequestSignature() {
    $headers = getallheaders();
    if (empty($headers['X-Hub-Signature'])) {
      return false;
    }

    // Get payload
    $payload = file_get_contents('php://input');

    $api_key = variable_get('pagarme_api_key');
    $sdk = new \Drupal\pagarme\PagarmeSdk($api_key);
    return $sdk->pagarme->postback()->validateRequest($payload, $headers['X-Hub-Signature']);
  }
}
