<?php

namespace Drupal\pagarme\Commerce\FormControllers;

class PagarmeCheckoutPagarmeForm {

  public static function getDefinition($payment_method, $pane_values, $checkout_pane, $order) {

    $pagarmeDrupal = new \Drupal\pagarme\PagarmeDrupal();
    $pagarmeDrupal->setOrder($order);

    $config = $pagarmeDrupal->getPagarmeConfig();

    $checkout_params = $pagarmeDrupal->processOrder();

    $commerce_order = commerce_order_load($order->order_number);

    if ($config['pagarme_customer_data']) {
      if (!empty($config['pagarme_cpf_token'])) {
        $checkout_params['customerDocumentNumber'] = token_replace($config['pagarme_cpf_token'], array('commerce-order' => $commerce_order));
      } else if (!empty($config['pagarme_cnpj_token'])) {
        $checkout_params['customerDocumentNumber'] = token_replace($config['pagarme_cnpj_token'], array('commerce-order' => $commerce_order));
      }

      $checkout_params['customerAddressStreetNumber'] = token_replace($config['pagarme_street_number_token'], array('commerce-order' => $commerce_order));

      $checkout_params['customerPhoneDdd'] = token_replace($config['pagarme_phone_ddd_token'], array('commerce-order' => $commerce_order));

      $checkout_params['customerPhoneNumber'] = token_replace($config['pagarme_phone_number_token'], array('commerce-order' => $commerce_order));

      if ($checkout_params['paymentMethods'] == 'boleto') {
        $boleto_discount_amount = (int) $checkout_params['boletoDiscountAmount'];
        $boleto_amount = (int) $checkout_params['amount'];
        if ($boleto_discount_amount > $boleto_amount) {
          watchdog('pagarme_error', t('Ticket discount amount greater than transaction value: <pre>@pre</pre>'), array('@pre' => print_r($checkout_params, TRUE)), WATCHDOG_ERROR);
        }
      }
    }

    drupal_add_js('https://assets.pagar.me/checkout/checkout.js', 'external');

    drupal_add_css(drupal_get_path('module', 'pagarme') . '/assets/stylesheets/pagarme.css', 'file');

    $settings = array(
      'pagarme' => array(
        'encryption_key' => variable_get('pagarme_encryption_key', ''),
        'checkout_params' => $checkout_params,
      ),
    );
    drupal_add_js($settings, array('type' => 'setting'));

    $form = array();

    $form['#attached']['js'][] = array(
        'data' => drupal_get_path('module', 'pagarme') . '/assets/js/pagarme.js',
        'type' => 'file',
    );

    $form['pagarme_cp'] = array(
      '#type' => 'container'
    );

    $form['pagarme_cp']['answer'] = array(
      '#attributes' => array('class' => 'pagarme-cp-answer'),
      '#type' => 'hidden',
    );

    $form['pagarme_cp']['messages'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="pagarme-cp-messages"></div>',
    );

    $form['pagarme_cp']['pay_button'] = array(
      '#type' => 'link',
      '#title' => variable_get('pagarme_display_title_pay_button', 'Pagar'),
      '#href' => '',
      '#attributes' => array('id' => array('pagarme-pay-button')),
      '#options' => array(
        'fragment' => null,
        'html' => TRUE,
        'external' => TRUE
      ),
    );
    return $form;
  }

  public static function submit($payment_method, $pane_form, $pane_values, $order, $charge) {
    try {
      $pagarme_answer = json_decode($pane_values['pagarme_cp']['answer']);

      if (variable_get('pagarme_debug', FALSE)) {
        watchdog('pagarme_debug', t('@payment_way: <pre>@pre</pre>'), array('@pre' => print_r($pagarme_answer, TRUE)), WATCHDOG_DEBUG);
      }

      if (empty($pagarme_answer)) {
        global $user;
        $error_msg = print_r($user, TRUE)
          . print_r($order, TRUE)
          . print_r($pane_values, TRUE)
          . print_r($pagarme_answer, TRUE);
        throw new \ErrorException('Invalid Pagar.me checkout response: <pre>' . $error_msg . '</pre>');
      }

      $order->data['pagarme_payment_method'] = $pagarme_answer->payment_method;

      $pagarme_api_key = variable_get('pagarme_api_key');
      $pagarmeDrupal = new \Drupal\pagarme\PagarmeDrupal($pagarme_api_key);
      $pagarmeDrupal->setOrder($order);

      $config = $pagarmeDrupal->getPagarmeConfig();
      $commerce_order = commerce_order_load($order->order_number);

      // Dados do cliente
      $customer = array();
      if (!empty($pagarme_answer->customer)) {
        $customer = (array)$pagarme_answer->customer;
        if (!empty($customer['address'])) {
          $customer['address'] = (array) $customer['address'];
        }
        if (!empty($customer['phone'])) {
          $customer['phone'] = (array) $customer['phone'];
        }
      }

      if (empty($customer['address']['street_number'])) {
        if (!empty($config['pagarme_street_number_token'])) {
          $customer['address']['street_number'] = token_replace($config['pagarme_street_number_token'], array('commerce-order' => $commerce_order));
        }
      }

      if (empty($customer['document_number'])) {
        if (!empty($config['pagarme_cpf_token'])) {
          $customer['document_number'] = token_replace($config['pagarme_cpf_token'], array('commerce-order' => $commerce_order));
        } else if (!empty($config['pagarme_cnpj_token'])) {
          $customer['document_number'] = token_replace($config['pagarme_cnpj_token'], array('commerce-order' => $commerce_order));
        }
      }

      if (empty($customer['phone']['ddd'])) {
        if (!empty($config['pagarme_phone_ddd_token'])) {
          $customer['phone']['ddd']= token_replace($config['pagarme_phone_ddd_token'], array('commerce-order' => $commerce_order));
        }
      }

      if (empty($customer['phone']['number'])) {
        if (!empty($config['pagarme_phone_number_token'])) {
          $customer['phone']['number'] = token_replace($config['pagarme_phone_number_token'], array('commerce-order' => $commerce_order));
        }
      }

      if (empty($customer['gender'])) {
        if (!empty($config['pagarme_gender_token'])) {
          $customer['gender'] = token_replace($config['pagarme_gender_token'], array('commerce-order' => $commerce_order));
        }
      }

      if (empty($customer['pagarme_birthday_token'])) {
        if (!empty($config['pagarme_birthday_token'])) {
          $customer_born_at = token_replace($config['pagarme_birthday_token'], array('commerce-order' => $commerce_order));
          $customer['pagarme_birthday_token'] = date('m-d-Y', strtotime($customer_born_at));
        }
      }

      drupal_alter('pagarme_customer', $customer, $order);
      $pagarmeDrupal->setCustomer($customer);

      foreach (module_implements('pagarme_checkout_answer') as $module) {
        $function = $module . '_pagarme_checkout_answer';
        $function($pagarme_answer, $order);
      }
      switch ($pagarme_answer->payment_method) {

        case 'credit_card':
          $amount = $pagarme_answer->amount;
          $card_hash = $pagarme_answer->card_hash;
          $installments = $pagarme_answer->installments;
          $transaction = $pagarmeDrupal->pagarmeCreditCardTransaction($amount, $card_hash, $installments);
          break;

        case 'boleto':
          $transaction = $pagarmeDrupal->pagarmeBoletoTransaction($pagarme_answer->amount);
          $order->data['pagarme_payment_boleto_url'] = $transaction->getBoletoUrl();

          commerce_order_status_update(
            $order, COMMERCE_PAYMENT_STATUS_PENDING, FALSE, TRUE
            , t('Order was processed by pagarme_cp with bankbillet and is waiting for response from Pagar.me')
          );
          break;
      }

      foreach (module_implements('pagarme_transaction_post_process') as $module) {
        $function = $module . '_pagarme_transaction_post_process';
        $function($transaction, $order);
      }

      $pagarme_data = array(
        'pagarme_id' => $transaction->getId(),
        'payment_method' => $transaction->getPaymentMethod(),
        'amount' => $transaction->getAmount(),
        'payment_status' => $transaction->getStatus(),
        'order_id' => $order->order_id,
        'consumer_email' => $order->mail,
      );
      $pagarmePostback = new \Drupal\pagarme\Entity\PagarmePostback($pagarme_data);
      $pagarmePostback->processPagarmeData();
      $order->data['pagarme_payment_transaction_id'] = $transaction->getId();

      commerce_order_save($order);
      drupal_goto('checkout/' . $order->order_id . '/complete');
    } catch (\Exception $e) {
      watchdog_exception('pagarme_error', $e);
      drupal_set_message(t('There was an error with Pagar.me. Please try again later.'), 'error');
      drupal_goto('checkout/' . $order->order_id . '/review');
    }
  }

}
