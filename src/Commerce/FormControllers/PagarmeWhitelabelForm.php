<?php

namespace Drupal\pagarme\Commerce\FormControllers;

class PagarmeWhitelabelForm {

  public static function getDefinition($payment_method, $pane_values, $checkout_pane, $order) {

    $pagarme_api_key = variable_get('pagarme_api_key');
    $pagarmeDrupal = new \Drupal\pagarme\PagarmeDrupal($pagarme_api_key);
    $pagarmeDrupal->setOrder($order);

    $config = $pagarmeDrupal->getPagarmeConfig();

    drupal_add_js('https://assets.pagar.me/js/pagarme.min.js', 'external');

    $settings = array(
      'pagarme' => array(
        'encryption_key' => variable_get('pagarme_encryption_key', ''),
      ),
    );
    drupal_add_js($settings, array('type' => 'setting'));

    $form = array();
    $default = array(
      'type' => '',
      'number' => '',
      'exp_month' => date('m'),
      'exp_year' => date('Y'),
    );

    $current_year_2 = date('y');
    $current_year_4 = date('Y');

   $form['pagarme_whitelabel'] = array(
      '#type' => 'container',
      '#tree' => TRUE,
      '#attached' => array(
        'css' => array(drupal_get_path('module', 'commerce_payment') . '/theme/commerce_payment.theme.css'),
        'js' => array('data' => drupal_get_path('module', 'pagarme') . '/assets/js/pagarme.js',
        'type' => 'file'),
      ),
    );

    $form['pagarme_whitelabel']['answer'] = array(
      '#attributes' => array('class' => 'pagarme-cp-answer'),
      '#type' => 'hidden',
    );

    $form['pagarme_whitelabel']['messages'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="pagarme-cp-messages"></div>',
    );

    // Meios de pagamento dispofignoníveis no Checkout.
    $payment_methods = $config['pagarme_payment_methods'];
    $payment_methods = array_filter($payment_methods);

    if (count($payment_methods) > 1) {
      $form['pagarme_whitelabel']['choose_method'] = array(
        '#type' => 'radios',
        '#title' => t('Escolha o método de pagamento'),
        '#options' => array('credit_card' => 'Cartão de crédito', 'boleto' => 'Boleto'),
        '#required' => TRUE,
      );
    } elseif (count($payment_methods) == 1) {
      $form['pagarme_whitelabel']['choose_method'] = array(
        '#type' => 'hidden',
        '#value' => current($payment_methods),
      );
    }

    if (in_array('credit_card', $payment_methods)) {

      $form['pagarme_whitelabel']['credit_card'] = array(
        '#type' => 'fieldset',
        '#title' => t('Pagamento com cartão de crédito'),
        '#states' => array(
          'visible' => array('input[name="commerce_payment[payment_details][pagarme_whitelabel][choose_method]"]' => array('value' => 'credit_card')),
        ),
      );

      // Mensagem opcional que aparecerá embaixo do botão de pagamento Cartão de Crédito.
      $form['pagarme_whitelabel']['credit_card']['helper_text'] = array(
        '#type' => 'markup',
        '#markup' => '<div class="helper-text">' . $config['pagarme_credit_card_helper_text'] . '</div>',
      );

      $card_brands = \Drupal\pagarme\Helpers\PagarmeUtility::cardBrands();
      $form['pagarme_whitelabel']['credit_card']['type'] = array(
        '#type' => 'select',
        '#title' => t('Card type'),
        '#options' => array_intersect_key($card_brands, array_filter($config['pagarme_card_brands'])),
        '#default_value' => $default['type'],
        '#states' => array(
          'required' => array('input[name="commerce_payment[payment_details][pagarme_whitelabel][choose_method]"]' => array('value' => 'credit_card')),
        )
      );

      $form['pagarme_whitelabel']['credit_card']['number'] = array(
        '#type' => 'textfield',
        '#title' => t('Número do cartão'),
        '#default_value' => $default['number'],
        '#attributes' => array('autocomplete' => 'off', 'pattern' => '[0-9]*'),
        '#maxlength' => 19,
        '#size' => 20,
        '#states' => array(
          'required' => array('input[name="commerce_payment[payment_details][pagarme_whitelabel][choose_method]"]' => array('value' => 'credit_card')),
        )
      );

      $form['pagarme_whitelabel']['credit_card']['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Nome (como escrito no cartão)'),
        '#attributes' => array('autocomplete' => 'off'),
        '#states' => array(
          'required' => array('input[name="commerce_payment[payment_details][pagarme_whitelabel][choose_method]"]' => array('value' => 'credit_card')),
        )
      );

      // Always add fields for the credit card expiration date.
      $form['pagarme_whitelabel']['credit_card']['month_expiration'] = array(
        '#type' => 'select',
        '#title' => t('Expiration'),
        '#options' => drupal_map_assoc(array_keys(commerce_months())),
        '#default_value' => strlen($default['exp_month']) == 1 ? '0' . $default['exp_month'] : $default['exp_month'],
        '#prefix' => '<div class="commerce-credit-card-expiration">',
        '#suffix' => '<span class="commerce-month-year-divider">/</span>',
        '#states' => array(
          'required' => array('input[name="commerce_payment[payment_details][pagarme_whitelabel][choose_method]"]' => array('value' => 'credit_card')),
        )
      );

      // Build a year select list that uses a 4 digit key with a 2 digit value.
      $options = array();

      for ($i = 0; $i < 20; $i++) {
        $options[$current_year_4 + $i] = str_pad($current_year_2 + $i, 2, '0', STR_PAD_LEFT);
      }

      $form['pagarme_whitelabel']['credit_card']['year_expiration'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $default['exp_year'],
        '#suffix' => '</div>',
        '#states' => array(
          'required' => array('input[name="commerce_payment[payment_details][pagarme_whitelabel][choose_method]"]' => array('value' => 'credit_card')),
        )
      );

      $form['pagarme_whitelabel']['credit_card']['cvv'] = array(
        '#type' => 'textfield',
        '#title' => t('Código de segurança'),
        '#size' => 5,
        '#maxlength' => 3,
        '#attributes' => array('autocomplete' => 'off', 'pattern' => '[0-9]*'),
        '#states' => array(
          'required' => array('input[name="commerce_payment[payment_details][pagarme_whitelabel][choose_method]"]' => array('value' => 'credit_card')),
        )
      );

      $max_installments = $pagarmeDrupal->getCreditCardMaxInstallments();
      if ($max_installments > 1) {
        $options = array();
        for ($i=1; $i <= $max_installments; $i++) { 
          $options[$i] = $i;
        }
        $installments_amount = $pagarmeDrupal->calculateInstallmentsAmount();
        $default_installment = $config['pagarme_default_installment'];
        if (!empty($pane_values['payment_details']['pagarme_whitelabel']['credit_card']['installments'])) {
          $default_installment = $pane_values['payment_details']['pagarme_whitelabel']['credit_card']['installments'];
        }
        $portion = ' x ' . pagarme_currency_format($installments_amount[$default_installment]['installment_amount']);
        $total = "Total: " . pagarme_currency_format($installments_amount[$default_installment]['total_amount']);

        $form['pagarme_whitelabel']['credit_card']['installments'] = array(
          '#type' => 'select',
          '#title' => t('parcelar em'),
          '#options' => $options,
          '#default_value' => $default_installment,
          '#field_suffix' => $portion,
          '#ajax' => array(
            'callback' => array(new \Drupal\pagarme\Commerce\FormControllers\PagarmeWhitelabelForm(), 'pagarme_installments_ajax_callback'),
            'wrapper' => 'pagarme-credit-card-installments',
            'method' => 'replace',
            'effect' => 'fade',
          ),
          '#prefix' => '<div id="pagarme-credit-card-installments">',
          '#suffix' => '<span>'.$total.'</span></div>',
          '#states' => array(
            'required' => array('input[name="commerce_payment[payment_details][pagarme_whitelabel][choose_method]"]' => array('value' => 'credit_card')),
          )
        );
      } else {
        $form['pagarme_whitelabel']['credit_card']['installments'] = array(
          '#type' => 'hidden',
          '#value' => 1,
        );
      }

      $form['pagarme_whitelabel']['credit_card']['pay_button'] = array(
        '#type' => 'link',
        '#title' => variable_get('pagarme_display_title_pay_button', 'Pagar'),
        '#href' => '',
        '#attributes' => array('id' => array('pagarme-whitelabel-credit-card-pay-button')),
        '#options' => array(
          'fragment' => null,
          'html' => TRUE,
          'external' => TRUE
        ),
      );
    }

    if (in_array('credit_card', $payment_methods)) {

      $form['pagarme_whitelabel']['bankbillet'] = array(
        '#type' => 'fieldset',
        '#title' => t('Pagamento com boleto'),
        '#states' => array(
          'visible' => array('input[name="commerce_payment[payment_details][pagarme_whitelabel][choose_method]"]' => array('value' => 'boleto')),
        ),
      );

      // Mensagem opcional que aparecerá embaixo do botão de pagamento Boleto.
      $form['pagarme_whitelabel']['bankbillet']['helper_text'] = array(
        '#type' => 'markup',
        '#markup' => '<div class="helper-text">' . $config['pagarme_boleto_helper_text'] . '</div>',
      );
  
      $form['pagarme_whitelabel']['bankbillet']['pay_button'] = array(
        '#type' => 'link',
        '#title' => variable_get('pagarme_display_title_pay_button', 'Pagar'),
        '#href' => '',
        '#attributes' => array('id' => array('pagarme-whitelabel-bankbillet-pay-button')),
        '#options' => array(
          'fragment' => null,
          'html' => TRUE,
          'external' => TRUE
        ),
      );
    }

    return $form;
  }

  public static function pagarme_installments_ajax_callback($form, &$form_state) {
    return $form['commerce_payment']['payment_details']['pagarme_whitelabel']['credit_card']['installments'];
  }

  public static function submit($payment_method, $pane_form, $pane_values, $order, $charge) {
    try {
      $pagarme_answer = json_decode($pane_values['pagarme_whitelabel']['answer']);

      if (variable_get('pagarme_debug', FALSE)) {
        watchdog('pagarme_debug', t('@payment_way: <pre>@pre</pre>'), array('@pre' => print_r($pagarme_answer, TRUE)), WATCHDOG_DEBUG);
      }

      if (empty($pagarme_answer)) {
        global $user;
        $error_msg = print_r($user, TRUE)
          . print_r($order, TRUE)
          . print_r($pane_values, TRUE)
          . print_r($pagarme_answer, TRUE);
        throw new \ErrorException('Invalid Pagar.me checkout response: <pre>' . $error_msg . '</pre>');
      }

      $payment_info = $pane_values['pagarme_whitelabel'];
      $order->data['pagarme_payment_method'] = $payment_info['choose_method'];

      $pagarme_api_key = variable_get('pagarme_api_key');
      $pagarmeDrupal = new \Drupal\pagarme\PagarmeDrupal($pagarme_api_key);
      $pagarmeDrupal->setOrder($order);

      $config = $pagarmeDrupal->getPagarmeConfig();
      $commerce_order = commerce_order_load($order->order_number);

      // Dados do cliente
      $customer = array();
      $customer['address'] = array();
      $customer['phone'] = array();
      if (!empty($config['pagarme_street_number_token'])) {
        $customer['address']['street_number'] = token_replace($config['pagarme_street_number_token'], array('commerce-order' => $commerce_order));
      }

      if (!empty($config['pagarme_cpf_token'])) {
        $customer['document_number'] = token_replace($config['pagarme_cpf_token'], array('commerce-order' => $commerce_order));
      } else if (!empty($config['pagarme_cnpj_token'])) {
        $customer['document_number'] = token_replace($config['pagarme_cnpj_token'], array('commerce-order' => $commerce_order));
      }

      if (!empty($config['pagarme_phone_ddd_token'])) {
        $customer['phone']['ddd'] = token_replace($config['pagarme_phone_ddd_token'], array('commerce-order' => $commerce_order));
      }

      if (!empty($config['pagarme_phone_number_token'])) {
        $customer['phone']['number'] = token_replace($config['pagarme_phone_number_token'], array('commerce-order' => $commerce_order));
      }

      if (!empty($config['pagarme_gender_token'])) {
        $customer['gender'] = token_replace($config['pagarme_gender_token'], array('commerce-order' => $commerce_order));
      }

      if (!empty($config['pagarme_birthday_token'])) {
        $customer_born_at = token_replace($config['pagarme_birthday_token'], array('commerce-order' => $commerce_order));
        $customer['born_at'] = date('m-d-Y', strtotime($customer_born_at));
      }

      drupal_alter('pagarme_customer', $customer, $order);
      $pagarmeDrupal->setCustomer($customer);

      switch ($payment_info['choose_method']) {

        case 'credit_card':
          $card_hash = $pagarme_answer->card_hash;
          $installments = $pane_values['pagarme_whitelabel']['credit_card']['installments'];
          $amount = $pagarmeDrupal->calculateCreditCardAmount($installments);
          $transaction = $pagarmeDrupal->pagarmeCreditCardTransaction($amount, $card_hash, $installments);
          break;

        case 'boleto':
          $amount = $pagarmeDrupal->calculateBoletoAmount();
          $transaction = $pagarmeDrupal->pagarmeBoletoTransaction($amount);
          $order->data['pagarme_payment_boleto_url'] = $transaction->getBoletoUrl();

          commerce_order_status_update(
            $order, COMMERCE_PAYMENT_STATUS_PENDING, FALSE, TRUE
            , t('Order was processed by pagarme_cp with bankbillet and is waiting for response from Pagar.me')
          );
          break;
      }

      foreach (module_implements('pagarme_transaction_post_process') as $module) {
        $function = $module . '_pagarme_transaction_post_process';
        $function($transaction, $order);
      }

      $pagarme_data = array(
        'pagarme_id' => $transaction->getId(),
        'payment_method' => $transaction->getPaymentMethod(),
        'amount' => $transaction->getAmount(),
        'payment_status' => $transaction->getStatus(),
        'order_id' => $order->order_id,
        'consumer_email' => $order->mail,
      );
      $pagarmePostback = new \Drupal\pagarme\Entity\PagarmePostback($pagarme_data);
      $pagarmePostback->processPagarmeData();
      $order->data['pagarme_payment_transaction_id'] = $transaction->getId();

      commerce_order_save($order);
      drupal_goto('checkout/' . $order->order_id . '/complete');
    } catch (\Exception $e) {
      watchdog_exception('pagarme_error', $e);
      drupal_set_message(t('There was an error with Pagar.me. Please try again later.'), 'error');
      drupal_goto('checkout/' . $order->order_id . '/review');
    }
  }

  public static function validate($details, $settings) {
    $credit_card = $details['credit_card'];
    $prefix = implode('][', $settings['form_parents']) . '][';
    $valid = TRUE;

    // if ($details['choose_method'])
    switch ($details['choose_method']) {
      case 'credit_card':
        // Validate the credit card type.
        if (!empty($credit_card['type'])) {

          // Validation is required for these cards
          $credit_card_exclusion_list = array('aura', 'diners', 'elo', 'hipercard');
          if (!in_array($credit_card['type'], $credit_card_exclusion_list)) {
            $card_brands = variable_get("pagarme_card_brands", array());
            $card_brands = array_filter($card_brands);
            $type = commerce_payment_validate_credit_card_type($credit_card['number'], $card_brands);

            if ($type === FALSE) {
              form_set_error($prefix . 'type', t('You have entered a credit card number of an unsupported card type.'));
              $valid = FALSE;
            }
            elseif ($type != $credit_card['type']) {
              form_set_error($prefix . 'number', t('You have entered a credit card number that does not match the type selected.'));
              $valid = FALSE;
            }
          }
        }

        // Validate the credit card number.
        if (!commerce_payment_validate_credit_card_number($credit_card['number'])) {
          form_set_error($prefix . 'number', t('You have entered an invalid credit card number.'));
          $valid = FALSE;
        }

        // Validate the expiration date.
        if (($invalid = commerce_payment_validate_credit_card_exp_date($credit_card['month_expiration'], $credit_card['year_expiration'])) !== TRUE) {
          form_set_error($prefix . $invalid  . '_expiration', t('You have entered an expired credit card.'));
          $valid = FALSE;
        }
        // Validate the security code if present.
        if (isset($credit_card['cvv']) && !commerce_payment_validate_credit_card_security_code($credit_card['number'], $credit_card['cvv'])) {
          form_set_error($prefix . 'cvv', t('You have entered an invalid card security code.'));
          $valid = FALSE;
        }
        break;
      case 'boleto':
        break;
    }
    return $valid;
  }
}
