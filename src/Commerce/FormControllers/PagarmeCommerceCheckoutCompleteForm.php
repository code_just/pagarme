<?php

namespace Drupal\pagarme\Commerce\FormControllers;

class PagarmeCommerceCheckoutCompleteForm {

  public static function alter(&$form, &$form_state, $order) {

    $order_id = arg(1);
    $order = commerce_order_load($order_id);

    $pagarme_payment_methods = array('pagarme_cp|commerce_payment_pagarme_cp', 'pagarme_whitelabel|commerce_payment_pagarme_whitelabel');

    if (isset($order->data['payment_method']) && in_array($order->data['payment_method'], $pagarme_payment_methods)) {
      if ($order->data['pagarme_payment_method'] == 'boleto') {
      	$message = t("To complete your payment, <a href='!link' target='_blank'>click here</a> to open your billet in another browser window and complete your payment.", array('!link' => $order->data['pagarme_payment_boleto_url']));
        $form['pagarme_info'] = array(
          '#markup' => '<div class="pagarme-info">' . $message . '</div>'
        );
      }
    }
  }

}
