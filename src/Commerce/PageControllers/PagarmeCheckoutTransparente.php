<?php

namespace Drupal\pagarme\Commerce\PageControllers;

/**
 * Checkout Pagar.me
 */
class PagarmeCheckoutTransparente implements \Drupal\cool\Controllers\PageController {

  /**
   * Path to be used by hook_menu().
   */
  static public function getPath() {
    return 'pagarme/checkout';
  }

  /**
   * Passed to hook_menu()
   */
  static public function getDefinition() {
    return array(
      'title' => 'Pagar.me checkout',
      'description' => t('Checkout Pagar.me'),
    );
  }

  public static function pageCallback() {
    try {
      // $pagarme_server = variable_get('pagarme_server');
      // $pagarme_api_key = variable_get('pagarme_api_key');
      // $pagarme_encryption_key = variable_get('pagarme_encryption_key');

      // $pagarmeDrupal = new \Drupal\pagarme\PagarmeDrupal($pagarme_api_key);

      // $transaction = $pagarmeDrupal->boletoTransaction();

      // $transaction = $pagarmeDrupal->creditCardTransaction();
    }
    catch (Exception $e) {
      print $e->getMessage();
      drupal_exit();
    }
  }

  public static function accessCallback() {
    return TRUE;
  }

}
