<?php

namespace Drupal\pagarme\Controllers\FormControllers;

class PagarmeConfig extends \Drupal\cool\BaseSettingsForm {

  static public function getId() {
    return 'pagarme_settings_form';
  }

  static public function build() {
    $form = parent::build();
    $config = \Drupal\pagarme\Helpers\PagarmeUtility::getPagarmeConfig();
    $default_currency = commerce_default_currency();

    $form['basic_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configurações básicas')
    );
    $form['basic_settings']['pagarme_server'] = array(
      '#type' => 'select',
      '#title' => t('Servidor Pagar.me'),
      '#options' => array(
        'test' => t('TEST'),
        'live' => t('LIVE'),
      ),
      '#description' => t('TEST - Usar para testar, LIVE - Usar para o processamento de transações reais'),
      '#default_value' => $config['pagarme_server'],
      '#required' => TRUE,
    );
    $form['basic_settings']['pagarme_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Chave de API'),
      '#description' => t('Chave da API (disponível no seu dashboard)'),
      '#default_value' => $config['pagarme_api_key'],
      '#required' => TRUE,
    );
    $form['basic_settings']['pagarme_encryption_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Chave de criptografia'),
      '#description' => t('Chave de encriptação (disponível no seu dashboard)'),
      '#default_value' => $config['pagarme_encryption_key'],
      '#required' => TRUE,
    );
    $form['basic_settings']['pagarme_display_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Título exibido no método de pagamento'),
      '#description' => t('Texto utilizado na página de pagamento como descrição da forma de pagamento.'),
      '#default_value' => $config['pagarme_display_title'],
    );
    $form['basic_settings']['pagarme_display_title_pay_button'] = array(
      '#type' => 'textfield',
      '#title' => t('Título exibido no botão de pagamento'),
      '#description' => t('Texto usado no botão de pagamento (página de pagamento).'),
      '#default_value' => $config['pagarme_display_title_pay_button'],
    );
    $form['basic_settings']['pagarme_ui_color'] = array(
      '#type' => 'textfield',
      '#title' => t('Cor primária da interface de Checkout'),
      '#default_value' => $config['pagarme_ui_color'],
      '#description' => t('Cor primária da interface de Checkout'),
      '#required' => TRUE,
    );
    $form['basic_settings']['pagarme_debug'] = array(
      '#type' => 'checkbox',
      '#title' => t('Habilitar depuração de cada ação deste módulo'),
      '#default_value' => $config['pagarme_debug'],
    );
    $form['advanced_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configurações avançadas')
    );
    $form['advanced_settings']['order_display'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configurações de pedido'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );
    $order_display_settings = &$form['advanced_settings']['order_display'];
    if (module_exists('token')) {
      // $order_display_settings['pagarme_order_reason_token'] = array(
      //   '#type' => 'textfield',
      //   '#title' => t('Token de pedido'),
      //   '#description' => t('Qual o título que você deseja usar para os pedidos enviados para a Pagar.me. O padrão é "Pedido @order_number em @store". <strong>Você pode usar tokens</strong> aqui(veja o navegador de tokens abaixo).'),
      //   '#default_value' => $config['pagarme_order_reason_token'],
      // );

      $order_display_settings['pagarme_street_number_token'] = array(
        '#type' => 'textfield',
        '#title' => t('Token do número do imóvel'),
        '#description' => t('De onde você deseja obter os dados do campo "número do imóvel" para enviar para Pagar.me. <strong>Você pode usar tokens</strong> aqui(veja o navegador de tokens abaixo).'),
        '#default_value' => $config['pagarme_street_number_token'],
      );
      $order_display_settings['pagarme_phone_ddd_token'] = array(
        '#type' => 'textfield',
        '#title' => t('Token do DDD do telefone'),
        '#description' => t('De onde você deseja obter os dados do campo "DDD do telefone" para enviar para Pagar.me. <strong>Você pode usar tokens</strong> aqui(veja o navegador de tokens abaixo).'),
        '#default_value' => $config['pagarme_phone_ddd_token'],
      );
      $order_display_settings['pagarme_phone_number_token'] = array(
        '#type' => 'textfield',
        '#title' => t('Token do número de telefone'),
        '#description' => t('De onde você deseja obter os dados do campo "Telefone" para enviar para Pagar.me. <strong>Você pode usar tokens</strong> aqui(veja o navegador de tokens abaixo).'),
        '#default_value' => $config['pagarme_phone_number_token'],
      );
      // $order_display_settings['pagarme_name_token'] = array(
      //   '#type' => 'textfield',
      //   '#title' => t('Token de nome'),
      //   '#description' => t('De onde você deseja obter os dados de campo "Nome" para enviar para Pagar.me. <strong>Você pode usar tokens</strong> aqui(veja o navegador de tokens abaixo).'),
      //   '#default_value' => $config['pagarme_name_token'],
      // );
      $order_display_settings['pagarme_cpf_token'] = array(
        '#type' => 'textfield',
        '#title' => t('Token de CPF'),
        '#description' => t('De onde você deseja obter os dados de campo "CPF" para enviar para Pagar.me. <strong>Você pode usar tokens</strong> aqui(veja o navegador de tokens abaixo).'),
        '#default_value' => $config['pagarme_cpf_token'],
      );
      $order_display_settings['pagarme_cnpj_token'] = array(
        '#type' => 'textfield',
        '#title' => t('Token de CNPJ'),
        '#description' => t('De onde você deseja obter os dados de campo "CNPJ" para enviar para Pagar.me. <strong>Você pode usar tokens</strong> aqui(veja o navegador de tokens abaixo).'),
        '#default_value' => $config['pagarme_cnpj_token'],
      );
      $order_display_settings['pagarme_birthday_token'] = array(
        '#type' => 'textfield',
        '#title' => t('Token de data de nascimento'),
        '#description' => t('De onde você deseja obter os dados do campo "Data de nascimento" para enviar para Pagar.me. <strong>Você pode usar tokens</strong> aqui(veja o navegador de tokens abaixo).'),
        '#default_value' => $config['pagarme_birthday_token'],
      );
      $order_display_settings['pagarme_gender_token'] = array(
        '#type' => 'textfield',
        '#title' => t('Token de gênero(sexo)'),
        '#description' => t('De onde você deseja obter os dados do campo "Gênero(sexo)" para enviar para Pagar.me. <strong>Você pode usar tokens</strong> aqui(veja o navegador de tokens abaixo).'),
        '#default_value' => $config['pagarme_gender_token'],
      );
      $order_display_settings['tokens'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('commerce-order'),
      );
    }
    $form['advanced_settings']['payment_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configurações de pagamento'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );
    $payment_settings = &$form['advanced_settings']['payment_settings'];
    $payment_settings['pagarme_customer_data'] = array(
      '#type' => 'checkbox',
      '#title' => t('Capturar dados do cliente pelo Checkout?'),
      '#description' => t('Caso não deseje capturar dados do cliente pelo Checkout, desative essa opção'),
      '#default_value' => $config['pagarme_customer_data'],
    );
    $payment_settings['pagarme_disable_zero_document_number'] = array(
      '#type' => 'checkbox',
      '#title' => t('Não aceitar CPF ou CNPJ em que todos os números são zeros?'),
      '#default_value' => $config['pagarme_disable_zero_document_number'],
      '#states' => array(
        'visible' => array('input[name="pagarme_customer_data"]' => array('checked' => TRUE)),
      ),
    );

    $config['pagarme_payment_methods']['credit_card'] = 'credit_card';
    $payment_settings['pagarme_payment_methods'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Meios de pagamento disponíveis no Checkout'),
      '#description' => t('Meios de pagamento disponíveis no Checkout.'),
      '#options' => array('credit_card' => 'Cartão de Crédito', 'boleto' => 'Boleto'),
      '#default_value' => $config['pagarme_payment_methods'],
    );
    $payment_settings['pagarme_payment_methods']['credit_card']['#disabled'] = TRUE;

    $payment_settings['pagarme_max_installments'] = array(
      '#type' => 'select',
      '#title' => t('Número máximo de parcelas'),
      '#description' => t('Número máximo de parcelas aceitas.'),
      '#default_value' => $config['pagarme_max_installments'],
      '#options' => \Drupal\pagarme\Helpers\PagarmeUtility::installmentsNumber(),
      '#states' => array(
        'visible' => array('input[name="pagarme_payment_methods[credit_card]"]' => array('checked' => TRUE)),
      ),
    );
    $payment_settings['pagarme_default_installment'] = array(
      '#type' => 'select',
      '#title' => t('Parcela padrão selecionada ao abrir o checkout'),
      '#description' => t('Parcela padrão selecionada ao abrir o checkout.'),
      '#default_value' => $config['pagarme_default_installment'],
      '#options' => \Drupal\pagarme\Helpers\PagarmeUtility::installmentsNumber(),
      '#states' => array(
        'visible' => array('input[name="pagarme_payment_methods[credit_card]"]' => array('checked' => TRUE)),
      ),
    );
    $payment_settings['pagarme_interest_rate'] = array(
      '#type' => 'textfield',
      '#title' => t('Taxa de juros a ser cobrada na transação'),
      '#description' => t('Taxa de juros a ser cobrada na transação.'),
      '#default_value' => $config['pagarme_interest_rate'],
      '#element_validate' => array('element_validate_number'),
      '#size' => 15,
      '#states' => array(
        'visible' => array('input[name="pagarme_payment_methods[credit_card]"]' => array('checked' => TRUE)),
      ),
    );

    $pagarme_installment_start_value = commerce_currency_amount_to_decimal($config['pagarme_installment_start_value'], $default_currency);
    $payment_settings['pagarme_installment_start_value'] = array(
      '#type' => 'textfield',
      '#title' => t('Liberar parcelamento acima de'),
      '#description' => t('Libera o parcelamento para pedidos com valor acima do valor infomado.'),
      '#default_value' => $pagarme_installment_start_value,
      '#element_validate' => array('element_validate_number'),
      '#size' => 15,
      '#states' => array(
        'visible' => array('input[name="pagarme_payment_methods[credit_card]"]' => array('checked' => TRUE)),
      ),
    );
    $payment_settings['pagarme_free_installments'] = array(
      '#type' => 'select',
      '#title' => t('Número de parcelas que não terão juros cobrados'),
      '#description' => t('Número de parcelas que não terão juros cobrados.'),
      '#default_value' => $config['pagarme_free_installments'],
      '#options' => \Drupal\pagarme\Helpers\PagarmeUtility::installmentsNumber(),
      '#states' => array(
        'visible' => array('input[name="pagarme_payment_methods[credit_card]"]' => array('checked' => TRUE)),
      ),
    );
    $payment_settings['pagarme_card_brands'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Bandeiras aceitas pelo Checkout'),
      '#description' => t('Bandeiras aceitas pelo Checkout.'),
      '#options' => \Drupal\pagarme\Helpers\PagarmeUtility::cardBrands(),
      '#default_value' => $config['pagarme_card_brands'],
      '#states' => array(
        'visible' => array('input[name="pagarme_payment_methods[credit_card]"]' => array('checked' => TRUE)),
      ),
    );

    $payment_settings['pagarme_boleto_discount'] = array(
      '#type' => 'select',
      '#title' => t('Desconto boleto'),
      '#description' => t('Selecione o tipo de desconto para boleto'),
      '#options' => array('_none' => 'Nenhum', 'amount' => 'Desconto em centavos', 'percentage' => 'Percentual de desconto'),
      '#default_value' => $config['pagarme_boleto_discount'],
      // '#ajax' => array(
      //   'callback' => array(new \Drupal\pagarme\Controllers\FormControllers\PagarmeConfig(), '_pagarme_boleto_discount_ajax_callback'),
      //   'method' => 'replace',
      //   'effect' => 'fade',
      // ),
      '#states' => array(
        'visible' => array('input[name="pagarme_payment_methods[boleto]"]' => array('checked' => TRUE)),
      ),
    );

    $pagarme_boleto_discount_amount = commerce_currency_amount_to_decimal($config['pagarme_boleto_discount_amount'], $default_currency);
    $payment_settings['pagarme_boleto_discount_amount'] = array(
      '#type' => 'textfield',
      '#title' => t('Valor do desconto'),
      '#description' => t('Valor do desconto caso o meio de pagamento seja boleto. Ex: desconto de R$10,25 = 10.25.'),
      '#default_value' => $pagarme_boleto_discount_amount,
      '#element_validate' => array('element_validate_number'),
      '#prefix' => '<div id="boleto-discount-amount-options-replace">',
      '#suffix' => '</div>',
      '#states' => array(
        'visible' => array(
          'input[name="pagarme_payment_methods[boleto]"]' => array('checked' => TRUE),
          'select[name="pagarme_boleto_discount"]' => array('value' => 'amount'),
        ),
      ),
    );
    $payment_settings['pagarme_boleto_discount_percentage'] = array(
      '#type' => 'textfield',
      '#title' => t('Percentual de desconto'),
      '#description' => t('Percentual de desconto caso o meio de pagamento seja boleto. Ex: desconto de 25% = 25.'),
      '#default_value' => $config['pagarme_boleto_discount_percentage'],
      '#element_validate' => array('element_validate_integer_positive'),
      '#prefix' => '<div id="boleto-discount-percentage-options-replace">',
      '#suffix' => '</div>',
      '#maxlength' => 2,
      '#size' => 4,
      '#states' => array(
        'visible' => array(
          'input[name="pagarme_payment_methods[boleto]"]' => array('checked' => TRUE),
          'select[name="pagarme_boleto_discount"]' => array('value' => 'percentage'),
        ),
      ),
    );

    $pagarme_boleto_discount_start = commerce_currency_amount_to_decimal($config['pagarme_boleto_discount_start'], $default_currency);
    $payment_settings['pagarme_boleto_discount_start'] = array(
      '#type' => 'textfield',
      '#title' => t('Aplicar desconto a partir de'),
      '#description' => t('Irá aplicar desconto apenas em pedidos no qual o valor sejá maior que o valor informado. Ex: Aplicar desconto a partir de R$999,25 = 999.25.'),
      '#default_value' => $pagarme_boleto_discount_start,
      '#element_validate' => array('element_validate_number'),
      '#prefix' => '<div id="boleto-discount-amount-options-replace">',
      '#suffix' => '</div>',
      '#states' => array(
        'visible' => array(
          array(
            'input[name="pagarme_payment_methods[boleto]"]' => array('checked' => TRUE),
            'select[name="pagarme_boleto_discount"]' => array('value' => 'amount'),
          ),
          'xor',
          array(
            'input[name="pagarme_payment_methods[boleto]"]' => array('checked' => TRUE),
            'select[name="pagarme_boleto_discount"]' => array('value' => 'percentage'),
          ),
        ),
      ),
    );
    $payment_settings['pagarme_boleto_helper_text'] = array(
      '#type' => 'textarea',
      '#title' => t('Mensagem opcional boleto'),
      '#description' => t('Mensagem opcional que aparecerá embaixo do botão de pagamento Boleto.'),
      '#default_value' => $config['pagarme_boleto_helper_text'],
      '#states' => array(
        'visible' => array('input[name="pagarme_payment_methods[boleto]"]' => array('checked' => TRUE)),
      ),
    );
    $payment_settings['pagarme_credit_card_helper_text'] = array(
      '#type' => 'textarea',
      '#title' => t('Mensagem opcional cartão de Crédito'),
      '#description' => t('Mensagem opcional que aparecerá embaixo do botão de pagamento Cartão de Crédito.'),
      '#default_value' => $config['pagarme_credit_card_helper_text'],
      '#states' => array(
        'visible' => array('input[name="pagarme_payment_methods[credit_card]"]' => array('checked' => TRUE)),
      ),
    );
    return $form;
  }

  public function _pagarme_boleto_discount_ajax_callback($form, &$form_state) {
    $commands = array();
    $commands[] = ajax_command_replace(
        '#boleto-discount-amount-options-replace',
        render($form['advanced_settings']['payment_settings']['pagarme_boleto_discount_amount'])
    );
    $commands[] = ajax_command_replace(
        '#boleto-discount-percentage-options-replace',
        render($form['advanced_settings']['payment_settings']['pagarme_boleto_discount_percentage'])
    );
    return array('#type' => 'ajax','#commands' => $commands);
  }

  static public function validate($form, &$form_state) {
    $form_state['values']['pagarme_payment_methods']['credit_card'] = 'credit_card';
    $values =  &$form_state['values'];
    $values['pagarme_installment_start_value'] = commerce_currency_decimal_to_amount($values['pagarme_installment_start_value'], commerce_default_currency());
    if (!count(array_filter($values['pagarme_payment_methods']))) {
      form_set_error('pagarme_payment_methods', t('Meios de pagamento disponíveis no Checkout é necessário ativar no mínimo uma forma de pagamento.'));
    }

    if ($values['pagarme_payment_methods']['credit_card']) {
      if (!count(array_filter($values['pagarme_card_brands']))) {
        form_set_error('pagarme_card_brands', t('Bandeiras aceitas pelo Checkout é necessário ativar no mínimo uma bandeira.'));
      }
    }
    if ($values['pagarme_payment_methods']['boleto']) {
      switch ($values['pagarme_boleto_discount']) {
        case 'amount':
          $discount_amount = &$values['pagarme_boleto_discount_amount'];
          if (empty($discount_amount)) {
            form_set_error('pagarme_boleto_discount_amount', t('Desconto em centavos field is required.'));
          } else {
            $discount_start = &$values['pagarme_boleto_discount_start'];
            if (empty($discount_start)) {
              form_set_error('pagarme_boleto_discount_start', t('Aplicar desconto a partir de field is required.'));
            } else {

              $discount_amount = commerce_currency_decimal_to_amount($discount_amount, commerce_default_currency());
              $discount_start = commerce_currency_decimal_to_amount($discount_start, commerce_default_currency());

              if ($discount_amount >= $discount_start) {
                form_set_error('pagarme_boleto_discount_amount', t('O valor do desconto em centavos é maior/igual ao valor mínimo para desconto.'));
              }
            }
          }
          break;
        case 'percentage':
          if (empty($values['pagarme_boleto_discount_percentage'])) {
            form_set_error('pagarme_boleto_discount_percentage', t('Percentual de desconto field is required.'));
          }
          break;
      }
    }
  }
}
