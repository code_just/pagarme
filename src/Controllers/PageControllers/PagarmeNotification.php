<?php

namespace Drupal\pagarme\Controllers\PageControllers;

/**
 * Process the return data that Pagar.me
 */
class PagarmeNotification implements \Drupal\cool\Controllers\PageController {

  /**
   * Path to be used by hook_menu().
   */
  static public function getPath() {
    return 'pagarme/notification';
  }

  /**
   * Passed to hook_menu()
   */
  static public function getDefinition() {
    return array(
      'title' => 'Pagar.me',
      'description' => t('Pagar.me notification'),
    );
  }

  public static function pageCallback() {
    try {
      $pagarmePostback = \Drupal\pagarme\Entity\PagarmePostback::create($_POST);
      $pagarmePostback->processPagarmeData();
    }
    catch (Exception $e) {
      print $e->getMessage();
      drupal_exit();
    }
  }

  public static function accessCallback() {
    return TRUE;
  }

}
