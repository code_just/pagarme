<?php

namespace Drupal\pagarme\Controllers\PageControllers;

class PagarmeConfig implements \Drupal\cool\Controllers\PageController {

  /**
   * Path to be used by hook_menu().
   */
  static public function getPath() {
    return 'admin/config/services/pagarme';
  }

  /**
   * Passed to hook_menu()
   */
  static public function getDefinition() {
    return array(
      'title' => 'Pagar.me',
      'description' => t('Basic configuration to integrate Drupal with Pagar.me'),
    );
  }

  public static function pageCallback() {
    return drupal_get_form('pagarme_settings_form');
  }

  public static function accessCallback() {
    return user_access('administer pagarme');
  }

}
