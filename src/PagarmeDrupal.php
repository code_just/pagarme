<?php

namespace Drupal\pagarme;

use Drupal\pagarme\PagarmeSdk as PagarmeSdk;

use PagarMe\Sdk\Customer\Address;
use PagarMe\Sdk\Card\Card;
use PagarMe\Sdk\Customer\Customer;
use PagarMe\Sdk\Customer\Phone;
use PagarMe\Sdk\Recipient\Recipient;
use PagarMe\Sdk\SplitRule\SplitRuleCollection;

/**
 * @file Class encapsulating the business logic related to integrate Pagar.me with
 * Drupal structures.
 */
class PagarmeDrupal extends PagarmeSdk {

  protected $postback_url;

  protected $order;
  protected $order_wrapper;
  protected $customer;
  protected $checkout_params;

  public function __construct($api_key = null) {
    parent::__construct($api_key);
    $this->postback_url = url('pagarme/notification', array('absolute' => TRUE));
  }

  public function setOrder($order) {
    $this->order = $order;
    $this->order_wrapper = entity_metadata_wrapper('commerce_order', $this->order);
  }

  /**
   *  É necessário passar os valores boolean em "$this->checkout_params" como string
   */
  public function processOrder() {

    // Cor primária da interface de Checkout
    $this->checkout_params['uiColor'] = $this->pagarme_config['pagarme_ui_color'];

    // Habilita a geração do token para autorização da transação. 
    //OBS: Caso você queira apenas pegar os dados do cliente, deixe esse atributo com o valor false, e realize a transação normalmente no seu backend, com os dados informados no formulário do checkout.
    $this->checkout_params['createToken'] = 'false';

    $amount_integer = $this->order_wrapper->commerce_order_total->amount->value();

    // Endereço da URL de POSTback do seu sistema, que receberá as notificações das alterações de status das transações
    $this->checkout_params['postbackUrl'] = $this->postback_url;

    // Valor da transação (em centavos) a ser capturada pelo Checkout. Ex: R$14,79 = 1479
    $this->checkout_params['amount'] = $amount_integer;

    $this->addCustomerData();
    $this->applyPaymentSettings();

    return $this->checkout_params;
  }

  protected function addCustomerData() {
    // Definição de capturar de dados do cliente pelo Checkout
    if (!$this->pagarme_config['pagarme_customer_data']) {
      $this->checkout_params['customerData'] = 'false';
    } else {
      // Não aceita CPF ou CNPJ em que todos os números são zeros, valor padrão false
      if ($this->pagarme_config['pagarme_disable_zero_document_number']) {
        $this->checkout_params['disableZeroDocumentNumber'] = 'true';
      }

      $this->checkout_params += array(
        'customerName' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->name_line->value(),
        'customerEmail' => $this->order_wrapper->mail->value(),
        'customerAddressStreet' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->thoroughfare->value(),
        'customerAddressComplementary' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->premise->value(),
        'customerAddressNeighborhood' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->dependent_locality->value(),
        'customerAddressCity' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->locality->value(),
        'customerAddressState' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->administrative_area->value(),
        'customerAddressZipcode' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->postal_code->value(),
      );
    }
  }

  protected function applyPaymentSettings() {

    // Meios de pagamento disponíveis no Checkout.
    $payment_methods = $this->pagarme_config['pagarme_payment_methods'];
    $this->checkout_params['paymentMethods'] = implode(',', array_filter($payment_methods));

    // Configurações cartão de crédito.
    if (in_array('credit_card', $payment_methods)) {

      // Bandeiras aceitas pelo Checkout.
      $card_brands = $this->pagarme_config['pagarme_card_brands'];
      $this->checkout_params['cardBrands'] = implode(',', array_filter($card_brands));

      // Número máximo de parcelas aceitas, de 1 a 12.
      $this->checkout_params['maxInstallments'] = $this->getCreditCardMaxInstallments();

      // Define a parcela padrão selecionada ao abrir o checkout.
      $this->checkout_params['defaultInstallment'] = $this->pagarme_config['pagarme_default_installment'];

      // Taxa de juros a ser cobrada na transação.
      if (!empty($this->pagarme_config['pagarme_interest_rate'])) {
        $this->checkout_params['interestRate'] = $this->pagarme_config['pagarme_interest_rate'];
      }

      // Número de parcelas que não terão juros cobrados.
      if (!empty($this->pagarme_config['pagarme_free_installments'])) {
        $this->checkout_params['freeInstallments'] = $this->pagarme_config['pagarme_free_installments'];
      }

      // Mensagem opcional que aparecerá embaixo do botão de pagamento Cartão de Crédito.
      if ($credit_card_helper_text = $this->pagarme_config['pagarme_credit_card_helper_text']) {
        $this->checkout_params['creditCardHelperText'] = $credit_card_helper_text;
      }
    }

    // Configurações boleto
    if (in_array('boleto', $payment_methods)) {

      // Desconto boleto (Percentual/Valor em centavos)
      $discount_type = $this->pagarme_config['pagarme_boleto_discount'];

      switch ($discount_type) {
        case 'amount':
          if ($boleto_discount_amount = $this->pagarme_config['pagarme_boleto_discount_amount']) {
            if ($this->applyDiscount()) {
              $this->checkout_params['boletoDiscountAmount'] = $boleto_discount_amount;              
            }
          }
          break;
        case 'percentage':
          if ($percentage = $this->pagarme_config['pagarme_boleto_discount_percentage']) {
            if ($this->applyDiscount()) {
              $this->checkout_params['boletoDiscountPercentage'] = $percentage;
            }
          }
          break;
      }

      // Mensagem opcional que aparecerá embaixo do botão de pagamento Boleto.
      if ($boleto_helper_text = $this->pagarme_config['pagarme_boleto_helper_text']) {
        $this->checkout_params['boletoHelperText'] = $boleto_helper_text;
      }
    }
  }

  public function getCreditCardMaxInstallments() {
    $max_installments = $this->pagarme_config['pagarme_max_installments'];

    // Validação para aplicar valor mínimo de pedido para parcelamento.
    if (!empty($this->pagarme_config['pagarme_installment_start_value'])) {
      $order_amount = $this->order_wrapper->commerce_order_total->amount->value();

      $installment_start_value = $this->pagarme_config['pagarme_installment_start_value'];

      // Desativar o parcelamento se o valor total do pedido for menor que o valor mínimo configurado.
      if ($order_amount < $installment_start_value) {
        $max_installments = 1;
      }
    }
    return $max_installments;
  }

  public function calculateInstallmentsAmount() {
    $amount = $this->order_wrapper->commerce_order_total->amount->value();
    // Taxa de juros a ser cobrada na transação.
    $interest_rate = 0;
    if (!empty($this->pagarme_config['pagarme_interest_rate'])) {
      $interest_rate = $this->pagarme_config['pagarme_interest_rate'];
    }

    // Número de parcelas que não terão juros cobrados.
    $free_installments = 1;
    if (!empty($this->pagarme_config['pagarme_free_installments'])) {
      $free_installments = $this->pagarme_config['pagarme_free_installments'];
    }

    // Valor máximo de parcelas.
    $max_installments = $this->getCreditCardMaxInstallments();
    return $this->pagarme->calculation()->calculateInstallmentsAmount(
        $amount,
        $interest_rate,
        $free_installments,
        $max_installments
    );
  }

  public function calculateBoletoAmount() {
    $order_amount = $this->order_wrapper->commerce_order_total->amount->value();
    if ($this->applyDiscount()) {
      $discount_amount = 0;
      // Desconto boleto (Percentual/Valor em centavos)
      switch ($this->pagarme_config['pagarme_boleto_discount']) {
        case 'amount':
          $discount_amount = $this->pagarme_config['pagarme_boleto_discount_amount'];
          break;
        case 'percentage':
          $discount_percentage = $this->pagarme_config['pagarme_boleto_discount_percentage'];
          if ($discount_percentage) {
            // Valor do desconto a ser aplicado
            $discount_amount = $order_amount * $discount_percentage / 100;
          }
          break;
      }
      return $order_amount - $discount_amount;
    }
    return $order_amount;
  }

  protected function applyDiscount() {
    // Valor do pedido
    $order_amount = $this->order_wrapper->commerce_order_total->amount->value();

    // Valor mínimo de pedido para aplicar desconto.Campo obrigatório apenas para descontos em centavos.
    $boleto_discount_start = $this->pagarme_config['pagarme_boleto_discount_start'];
    if (!empty($boleto_discount_start)) {
      // O desconto só vai ser aplicado se o valor do pedido for maior que o valor mínimo configurado para aplicar desconto.
      if ($order_amount > $boleto_discount_start) {
        return TRUE;
      }
      return FALSE;
    }
    return TRUE;
  }

  public function creditCardTransaction($pagarme_cp_answer) {
    $amount = $pagarme_cp_answer->amount;
    $card = new Card(array('hash' => $pagarme_cp_answer->card_hash));
    $customer = $this->dataCustomer($pagarme_cp_answer);
    $installments = $pagarme_cp_answer->installments;
    $capture = TRUE;
    $metadata = $this->transactionMetadata();
    $split_rules = $this->splitRuleCollection($pagarme_cp_answer);

    /** @var $transaction \PagarMe\Sdk\Transaction\CreditCardTransaction */
    return $this->pagarme->transaction()->creditCardTransaction(
        $amount,
        $card,
        $customer,
        $installments,
        $capture,
        $this->postback_url,
        $metadata,
        $split_rules
    );
  }

  public function pagarmeCreditCardTransaction($amount, $card_hash, $installments) {
    $card = new Card(array('hash' => $card_hash));
    $capture = TRUE;
    $metadata = $this->transactionMetadata();

    $obj = new \StdClass();
    $obj->payment_method = 'credit_card';
    $obj->amount = $amount;
    $split_rules = $this->splitRuleCollection($obj);

    /** @var $transaction \PagarMe\Sdk\Transaction\CreditCardTransaction */
    return $this->pagarme->transaction()->creditCardTransaction(
        $amount,
        $card,
        $this->customer,
        $installments,
        $capture,
        $this->postback_url,
        $metadata,
        $split_rules
    );
  }

  public function calculateCreditCardAmount($installments) {
    $installments_amount = $this->calculateInstallmentsAmount();
    return $installments_amount[$installments]['total_amount'];
  }

  public function boletoTransaction($pagarme_cp_answer) {
    $amount = $pagarme_cp_answer->amount;
    $customer = $this->dataCustomer($pagarme_cp_answer);
    $metadata = $this->transactionMetadata();
    $split_rules = $this->splitRuleCollection($pagarme_cp_answer);

    /** @var $transaction \PagarMe\Sdk\Transaction\BoletoTransaction */
    return $this->pagarme->transaction()->boletoTransaction(
        $amount,
        $customer,
        $this->postback_url,
        $metadata,
        $split_rules
    );
  }

  public function pagarmeBoletoTransaction($amount) {    
    $metadata = $this->transactionMetadata();

    $obj = new \StdClass();
    $obj->payment_method = 'boleto';
    $obj->amount = $amount;
    $split_rules = $this->splitRuleCollection($obj);

    /** @var $transaction \PagarMe\Sdk\Transaction\BoletoTransaction */
    return $this->pagarme->transaction()->boletoTransaction(
        $amount,
        $this->customer,
        $this->postback_url,
        $metadata,
        $split_rules
    );
  }

  public function splitRuleCollection($data) {
    $rules = array();
    if (module_exists('pagarme_marketplace')) {
      $split_rule_collection = new \Drupal\pagarme_marketplace\PagarmeSplitRuleCollection(
          $this->order,
          $data->payment_method,
          $data->amount
      );
      $rules = $split_rule_collection->doSplitRuleCollection();
    }
    return $rules;
  }

  public function transactionMetadata() {
    $metadata = array();
    $metadata = array(
      'order_id' => $this->order_wrapper->order_id->value(),
    );
    $order = $this->order;
    drupal_alter('pagarme_metadata', $metadata, $order);
    return $metadata;
  }

  /**
   * Set the customer property
   * @param array $customer
   * @return void
   */
  public function setCustomer($customer) {
    $customer_data = array();
    $data_address = array();
    $data_phone = array();

    // Commerce customer address
    $commerce_customer_address = $this->order_wrapper->commerce_customer_billing->commerce_customer_address;

    $customer_data['name'] = '';
    if (!empty($customer['name'])) {
      $customer_data['name'] = $customer['name']; 
    } else if (!empty($commerce_customer_address->name_line->value())) {
      $customer_data['name'] = $commerce_customer_address->name_line->value();
    }

    $customer_data['email'] = '';
    if (!empty($customer['email'])) {
      $customer_data['email'] = $customer['email']; 
    } else if (!empty($this->order_wrapper->mail->value())) {
      $customer_data['email'] = $this->order_wrapper->mail->value();
    }

    $data_address['street'] = '';
    if (!empty($customer['address']['street'])) {
      $data_address['street'] = $customer['address']['street']; 
    } else if (!empty($commerce_customer_address->thoroughfare->value())) {
      $data_address['street'] = $commerce_customer_address->thoroughfare->value();
    }

    $data_address['streetNumber'] = '';
    if (!empty($customer['address']['street_number'])) {
      $data_address['streetNumber'] = $customer['address']['street_number'];
    }

    $data_address['complementary'] = '';
    if (!empty($customer['address']['complementary'])) {
      $data_address['complementary'] = $customer['address']['complementary']; 
    } else if (!empty($commerce_customer_address->premise->value())) {
      $data_address['complementary'] = $commerce_customer_address->premise->value();
    }

    $data_address['neighborhood'] = '';
    if (!empty($customer['address']['neighborhood'])) {
      $data_address['neighborhood'] = $customer['address']['neighborhood']; 
    } else if (!empty($commerce_customer_address->dependent_locality->value())) {
      $data_address['neighborhood'] = $commerce_customer_address->dependent_locality->value();
    }

    $data_address['city'] = '';
    if (!empty($customer['address']['city'])) {
      $data_address['city'] = $customer['address']['city']; 
    } else if (!empty($commerce_customer_address->locality->value())) {
      $data_address['city'] = $commerce_customer_address->locality->value();
    }

    $data_address['state'] = '';
    if (!empty($customer['address']['state'])) {
      $data_address['state'] = $customer['address']['state']; 
    } else if (!empty($commerce_customer_address->administrative_area->value())) {
      $data_address['state'] = $commerce_customer_address->administrative_area->value();
    }

    $data_address['zipcode'] = '';
    if (!empty($customer['address']['zipcode'])) {
      $data_address['zipcode'] = $customer['address']['zipcode']; 
    } else if (!empty($commerce_customer_address->postal_code->value())) {
      $data_address['zipcode'] = $commerce_customer_address->postal_code->value();
    }

    $data_address['country'] = '';
    if (!empty($customer['address']['country'])) {
      $data_address['country'] = $customer['address']['country']; 
    } else if (!empty($commerce_customer_address->country->value())) {
      $data_address['country'] = $commerce_customer_address->country->value();
    }

    $data_phone = array(
      'ddd' => (!empty($customer['phone']['ddd'])) ? $customer['phone']['ddd'] : '',
      'number' => (!empty($customer['phone']['number'])) ? $customer['phone']['number'] : '',
      'ddi' => 55,
    );

    $customer_data += array(
      'document_number' => (!empty($customer['document_number'])) ? $customer['document_number'] : '',
      'gender' => (!empty($customer['gender'])) ? $customer['gender'] : '',
      'bornAt' => (!empty($customer['born_at'])) ? $customer['born_at'] : '',
    );

    $customer_data += array(
      'address' => new Address($data_address),
      'phone' => new Phone($data_phone),
    );

    $this->customer = new Customer($customer_data);
  }

  public function getCustomer() {
    return $this->customer;
  }

  public function dataCustomer($pagarme_cp_answer) {
    $customer_data = array();
    $data_address = array();
    $data_phone = array();

    $customer = $pagarme_cp_answer->customer;
    if (!empty($customer->address)) {
      $customer_data += array(
        'name' => $customer->name,
        'email' => $customer->email,
        'document_number' => $customer->document_number
      );

      $data_address = array(
        'street' => $customer->address->street,
        'neighborhood' => $customer->address->neighborhood,
        'zipcode' => $customer->address->zipcode,
        'complementary' => $customer->address->complementary,
        'city' => $customer->address->city,
        'state' => $customer->address->state,
        'country' => 'Brasil',
      );

    } else {
      $customer_data += array(
        'name' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->name_line->value(),
        'email' => $this->order_wrapper->mail->value(),
        'document_number' => $customer->document_number,
      );

      // Pagar.me needs the thoroughfare number to be separate from the street
      $thoroughfare = explode(',', $this->order_wrapper->commerce_customer_billing->commerce_customer_address->thoroughfare->value());

      $data_address += array(
        'street' => trim($thoroughfare[0]),
        'streetNumber' => trim($thoroughfare[1]),
        'complementary' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->premise->value(),
        'neighborhood' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->dependent_locality->value(),
        'city' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->locality->value(),
        'state' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->administrative_area->value(),
        'zipcode' => $this->order_wrapper->commerce_customer_billing->commerce_customer_address->postal_code->value(),
        'country' => 'Brasil',
      );      

    }

    $data_address['streetNumber'] = $customer->address->street_number;

    $data_phone = array(
      'ddd' => $customer->phone->ddd,
      'number' => $customer->phone->number,
      'ddi' => 55,
    );

    $customer_data += array(
      'gender' => $customer->gender,
      'bornAt' => $customer->born_at,
    );
    $customer_data += array(
      'address' => new Address($data_address),
      'phone' => new Phone($data_phone),
    );
    return new Customer($customer_data);
  }

  public function captureTransactions($pagarme_cp_answer) {
    $authorizedTransaction = $this->pagarme->transaction()->get($pagarme_cp_answer->token);

    $amount_integer = $this->order_wrapper->commerce_order_total->amount->value();
    return $this->pagarme->transaction()->capture($authorizedTransaction, $amount_integer);
  }
}
